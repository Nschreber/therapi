package com.holosystems.repository;

import com.holosystems.domain.Offers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Offers entity.
 */
@Repository
public interface OffersRepository extends JpaRepository<Offers, Long> {

    @Query(value = "select distinct offers from Offers offers left join fetch offers.states left join fetch offers.practicies",
        countQuery = "select count(distinct offers) from Offers offers")
    Page<Offers> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct offers from Offers offers left join fetch offers.states left join fetch offers.practicies")
    List<Offers> findAllWithEagerRelationships();

    @Query("select offers from Offers offers left join fetch offers.states left join fetch offers.practicies where offers.id =:id")
    Optional<Offers> findOneWithEagerRelationships(@Param("id") Long id);
}
