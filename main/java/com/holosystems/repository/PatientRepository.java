package com.holosystems.repository;

import com.holosystems.domain.Patient;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Patient entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    @Query("select patient from Patient patient where patient.defaultUser.login = ?#{principal.username}")
    List<Patient> findByDefaultUserIsCurrentUser();
}
