package com.holosystems.repository;

import com.holosystems.domain.Perscription;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Perscription entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PerscriptionRepository extends JpaRepository<Perscription, Long> {
}
