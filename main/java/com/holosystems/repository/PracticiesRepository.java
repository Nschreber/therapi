package com.holosystems.repository;

import com.holosystems.domain.Practicies;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Practicies entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PracticiesRepository extends JpaRepository<Practicies, Long> {
}
