package com.holosystems.repository;

import com.holosystems.domain.States;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the States entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StatesRepository extends JpaRepository<States, Long> {
}
