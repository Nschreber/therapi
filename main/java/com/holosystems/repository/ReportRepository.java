package com.holosystems.repository;

import com.holosystems.domain.Report;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Report entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

    @Query("select report from Report report where report.author.login = ?#{principal.username}")
    List<Report> findByAuthorIsCurrentUser();
}
