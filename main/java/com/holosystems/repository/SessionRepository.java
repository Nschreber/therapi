package com.holosystems.repository;

import com.holosystems.domain.Session;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Session entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {

    @Query("select session from Session session where session.user.login = ?#{principal.username}")
    List<Session> findByUserIsCurrentUser();
}
