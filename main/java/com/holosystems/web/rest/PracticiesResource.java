package com.holosystems.web.rest;

import com.holosystems.domain.Practicies;
import com.holosystems.repository.PracticiesRepository;
import com.holosystems.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.holosystems.domain.Practicies}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PracticiesResource {

    private final Logger log = LoggerFactory.getLogger(PracticiesResource.class);

    private static final String ENTITY_NAME = "practicies";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PracticiesRepository practiciesRepository;

    public PracticiesResource(PracticiesRepository practiciesRepository) {
        this.practiciesRepository = practiciesRepository;
    }

    /**
     * {@code POST  /practicies} : Create a new practicies.
     *
     * @param practicies the practicies to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new practicies, or with status {@code 400 (Bad Request)} if the practicies has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/practicies")
    public ResponseEntity<Practicies> createPracticies(@RequestBody Practicies practicies) throws URISyntaxException {
        log.debug("REST request to save Practicies : {}", practicies);
        if (practicies.getId() != null) {
            throw new BadRequestAlertException("A new practicies cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Practicies result = practiciesRepository.save(practicies);
        return ResponseEntity.created(new URI("/api/practicies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /practicies} : Updates an existing practicies.
     *
     * @param practicies the practicies to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated practicies,
     * or with status {@code 400 (Bad Request)} if the practicies is not valid,
     * or with status {@code 500 (Internal Server Error)} if the practicies couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/practicies")
    public ResponseEntity<Practicies> updatePracticies(@RequestBody Practicies practicies) throws URISyntaxException {
        log.debug("REST request to update Practicies : {}", practicies);
        if (practicies.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Practicies result = practiciesRepository.save(practicies);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, practicies.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /practicies} : get all the practicies.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of practicies in body.
     */
    @GetMapping("/practicies")
    public List<Practicies> getAllPracticies() {
        log.debug("REST request to get all Practicies");
        return practiciesRepository.findAll();
    }

    /**
     * {@code GET  /practicies/:id} : get the "id" practicies.
     *
     * @param id the id of the practicies to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the practicies, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/practicies/{id}")
    public ResponseEntity<Practicies> getPracticies(@PathVariable Long id) {
        log.debug("REST request to get Practicies : {}", id);
        Optional<Practicies> practicies = practiciesRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(practicies);
    }

    /**
     * {@code DELETE  /practicies/:id} : delete the "id" practicies.
     *
     * @param id the id of the practicies to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/practicies/{id}")
    public ResponseEntity<Void> deletePracticies(@PathVariable Long id) {
        log.debug("REST request to delete Practicies : {}", id);
        practiciesRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
