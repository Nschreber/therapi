/**
 * View Models used by Spring MVC REST controllers.
 */
package com.holosystems.web.rest.vm;
