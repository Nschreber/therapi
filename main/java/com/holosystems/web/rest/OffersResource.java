package com.holosystems.web.rest;

import com.holosystems.domain.Offers;
import com.holosystems.repository.OffersRepository;
import com.holosystems.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.holosystems.domain.Offers}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class OffersResource {

    private final Logger log = LoggerFactory.getLogger(OffersResource.class);

    private static final String ENTITY_NAME = "offers";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OffersRepository offersRepository;

    public OffersResource(OffersRepository offersRepository) {
        this.offersRepository = offersRepository;
    }

    /**
     * {@code POST  /offers} : Create a new offers.
     *
     * @param offers the offers to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new offers, or with status {@code 400 (Bad Request)} if the offers has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/offers")
    public ResponseEntity<Offers> createOffers(@RequestBody Offers offers) throws URISyntaxException {
        log.debug("REST request to save Offers : {}", offers);
        if (offers.getId() != null) {
            throw new BadRequestAlertException("A new offers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Offers result = offersRepository.save(offers);
        return ResponseEntity.created(new URI("/api/offers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /offers} : Updates an existing offers.
     *
     * @param offers the offers to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated offers,
     * or with status {@code 400 (Bad Request)} if the offers is not valid,
     * or with status {@code 500 (Internal Server Error)} if the offers couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/offers")
    public ResponseEntity<Offers> updateOffers(@RequestBody Offers offers) throws URISyntaxException {
        log.debug("REST request to update Offers : {}", offers);
        if (offers.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Offers result = offersRepository.save(offers);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, offers.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /offers} : get all the offers.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of offers in body.
     */
    @GetMapping("/offers")
    public List<Offers> getAllOffers(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Offers");
        return offersRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /offers/:id} : get the "id" offers.
     *
     * @param id the id of the offers to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the offers, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/offers/{id}")
    public ResponseEntity<Offers> getOffers(@PathVariable Long id) {
        log.debug("REST request to get Offers : {}", id);
        Optional<Offers> offers = offersRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(offers);
    }

    /**
     * {@code DELETE  /offers/:id} : delete the "id" offers.
     *
     * @param id the id of the offers to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/offers/{id}")
    public ResponseEntity<Void> deleteOffers(@PathVariable Long id) {
        log.debug("REST request to delete Offers : {}", id);
        offersRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
