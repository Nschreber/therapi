package com.holosystems.web.rest;

import com.holosystems.domain.Perscription;
import com.holosystems.repository.PerscriptionRepository;
import com.holosystems.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.holosystems.domain.Perscription}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PerscriptionResource {

    private final Logger log = LoggerFactory.getLogger(PerscriptionResource.class);

    private static final String ENTITY_NAME = "perscription";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PerscriptionRepository perscriptionRepository;

    public PerscriptionResource(PerscriptionRepository perscriptionRepository) {
        this.perscriptionRepository = perscriptionRepository;
    }

    /**
     * {@code POST  /perscriptions} : Create a new perscription.
     *
     * @param perscription the perscription to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new perscription, or with status {@code 400 (Bad Request)} if the perscription has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/perscriptions")
    public ResponseEntity<Perscription> createPerscription(@RequestBody Perscription perscription) throws URISyntaxException {
        log.debug("REST request to save Perscription : {}", perscription);
        if (perscription.getId() != null) {
            throw new BadRequestAlertException("A new perscription cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Perscription result = perscriptionRepository.save(perscription);
        return ResponseEntity.created(new URI("/api/perscriptions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /perscriptions} : Updates an existing perscription.
     *
     * @param perscription the perscription to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated perscription,
     * or with status {@code 400 (Bad Request)} if the perscription is not valid,
     * or with status {@code 500 (Internal Server Error)} if the perscription couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/perscriptions")
    public ResponseEntity<Perscription> updatePerscription(@RequestBody Perscription perscription) throws URISyntaxException {
        log.debug("REST request to update Perscription : {}", perscription);
        if (perscription.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Perscription result = perscriptionRepository.save(perscription);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, perscription.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /perscriptions} : get all the perscriptions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of perscriptions in body.
     */
    @GetMapping("/perscriptions")
    public List<Perscription> getAllPerscriptions() {
        log.debug("REST request to get all Perscriptions");
        return perscriptionRepository.findAll();
    }

    /**
     * {@code GET  /perscriptions/:id} : get the "id" perscription.
     *
     * @param id the id of the perscription to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the perscription, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/perscriptions/{id}")
    public ResponseEntity<Perscription> getPerscription(@PathVariable Long id) {
        log.debug("REST request to get Perscription : {}", id);
        Optional<Perscription> perscription = perscriptionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(perscription);
    }

    /**
     * {@code DELETE  /perscriptions/:id} : delete the "id" perscription.
     *
     * @param id the id of the perscription to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/perscriptions/{id}")
    public ResponseEntity<Void> deletePerscription(@PathVariable Long id) {
        log.debug("REST request to delete Perscription : {}", id);
        perscriptionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
