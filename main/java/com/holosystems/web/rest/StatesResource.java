package com.holosystems.web.rest;

import com.holosystems.domain.States;
import com.holosystems.repository.StatesRepository;
import com.holosystems.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.holosystems.domain.States}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class StatesResource {

    private final Logger log = LoggerFactory.getLogger(StatesResource.class);

    private static final String ENTITY_NAME = "states";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StatesRepository statesRepository;

    public StatesResource(StatesRepository statesRepository) {
        this.statesRepository = statesRepository;
    }

    /**
     * {@code POST  /states} : Create a new states.
     *
     * @param states the states to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new states, or with status {@code 400 (Bad Request)} if the states has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/states")
    public ResponseEntity<States> createStates(@RequestBody States states) throws URISyntaxException {
        log.debug("REST request to save States : {}", states);
        if (states.getId() != null) {
            throw new BadRequestAlertException("A new states cannot already have an ID", ENTITY_NAME, "idexists");
        }
        States result = statesRepository.save(states);
        return ResponseEntity.created(new URI("/api/states/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /states} : Updates an existing states.
     *
     * @param states the states to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated states,
     * or with status {@code 400 (Bad Request)} if the states is not valid,
     * or with status {@code 500 (Internal Server Error)} if the states couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/states")
    public ResponseEntity<States> updateStates(@RequestBody States states) throws URISyntaxException {
        log.debug("REST request to update States : {}", states);
        if (states.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        States result = statesRepository.save(states);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, states.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /states} : get all the states.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of states in body.
     */
    @GetMapping("/states")
    public List<States> getAllStates() {
        log.debug("REST request to get all States");
        return statesRepository.findAll();
    }

    /**
     * {@code GET  /states/:id} : get the "id" states.
     *
     * @param id the id of the states to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the states, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/states/{id}")
    public ResponseEntity<States> getStates(@PathVariable Long id) {
        log.debug("REST request to get States : {}", id);
        Optional<States> states = statesRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(states);
    }

    /**
     * {@code DELETE  /states/:id} : delete the "id" states.
     *
     * @param id the id of the states to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/states/{id}")
    public ResponseEntity<Void> deleteStates(@PathVariable Long id) {
        log.debug("REST request to delete States : {}", id);
        statesRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
