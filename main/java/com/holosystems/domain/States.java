package com.holosystems.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A States.
 */
@Entity
@Table(name = "states")
public class States implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "state_name")
    private String stateName;

    @ManyToMany(mappedBy = "states")
    @JsonIgnore
    private Set<Offers> offers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public States stateName(String stateName) {
        this.stateName = stateName;
        return this;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Set<Offers> getOffers() {
        return offers;
    }

    public States offers(Set<Offers> offers) {
        this.offers = offers;
        return this;
    }

    public States addOffers(Offers offers) {
        this.offers.add(offers);
        offers.getStates().add(this);
        return this;
    }

    public States removeOffers(Offers offers) {
        this.offers.remove(offers);
        offers.getStates().remove(this);
        return this;
    }

    public void setOffers(Set<Offers> offers) {
        this.offers = offers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof States)) {
            return false;
        }
        return id != null && id.equals(((States) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "States{" +
            "id=" + getId() +
            ", stateName='" + getStateName() + "'" +
            "}";
    }
}
