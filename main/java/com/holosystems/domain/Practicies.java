package com.holosystems.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Practicies.
 */
@Entity
@Table(name = "practicies")
public class Practicies implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "institutional_flag")
    private Integer institutionalFlag;

    @ManyToMany(mappedBy = "practicies")
    @JsonIgnore
    private Set<Offers> offers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Practicies name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInstitutionalFlag() {
        return institutionalFlag;
    }

    public Practicies institutionalFlag(Integer institutionalFlag) {
        this.institutionalFlag = institutionalFlag;
        return this;
    }

    public void setInstitutionalFlag(Integer institutionalFlag) {
        this.institutionalFlag = institutionalFlag;
    }

    public Set<Offers> getOffers() {
        return offers;
    }

    public Practicies offers(Set<Offers> offers) {
        this.offers = offers;
        return this;
    }

    public Practicies addOffers(Offers offers) {
        this.offers.add(offers);
        offers.getPracticies().add(this);
        return this;
    }

    public Practicies removeOffers(Offers offers) {
        this.offers.remove(offers);
        offers.getPracticies().remove(this);
        return this;
    }

    public void setOffers(Set<Offers> offers) {
        this.offers = offers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Practicies)) {
            return false;
        }
        return id != null && id.equals(((Practicies) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Practicies{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", institutionalFlag=" + getInstitutionalFlag() +
            "}";
    }
}
