package com.holosystems.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Report.
 */
@Entity
@Table(name = "report")
public class Report implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "text")
    private String text;

    @Column(name = "is_drafted")
    private Boolean isDrafted;

    @ManyToOne
    @JsonIgnoreProperties("reports")
    private User author;

    @ManyToOne
    @JsonIgnoreProperties("reports")
    private Patient patient;

    @ManyToOne
    @JsonIgnoreProperties("reports")
    private Doctor doctor;

    @ManyToOne
    @JsonIgnoreProperties("reports")
    private Perscription recipie;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Report title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public Report text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean isIsDrafted() {
        return isDrafted;
    }

    public Report isDrafted(Boolean isDrafted) {
        this.isDrafted = isDrafted;
        return this;
    }

    public void setIsDrafted(Boolean isDrafted) {
        this.isDrafted = isDrafted;
    }

    public User getAuthor() {
        return author;
    }

    public Report author(User user) {
        this.author = user;
        return this;
    }

    public void setAuthor(User user) {
        this.author = user;
    }

    public Patient getPatient() {
        return patient;
    }

    public Report patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public Report doctor(Doctor doctor) {
        this.doctor = doctor;
        return this;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Perscription getRecipie() {
        return recipie;
    }

    public Report recipie(Perscription perscription) {
        this.recipie = perscription;
        return this;
    }

    public void setRecipie(Perscription perscription) {
        this.recipie = perscription;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Report)) {
            return false;
        }
        return id != null && id.equals(((Report) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Report{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", text='" + getText() + "'" +
            ", isDrafted='" + isIsDrafted() + "'" +
            "}";
    }
}
