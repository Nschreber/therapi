package com.holosystems.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A Session.
 */
@Entity
@Table(name = "session")
public class Session implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "time_from")
    private Instant timeFrom;

    @Column(name = "time_to")
    private Instant timeTo;

    @Column(name = "notes")
    private String notes;

    @ManyToOne
    @JsonIgnoreProperties("sessions")
    private Patient patient;

    @ManyToOne
    @JsonIgnoreProperties("sessions")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("sessions")
    private Offers offer;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTimeFrom() {
        return timeFrom;
    }

    public Session timeFrom(Instant timeFrom) {
        this.timeFrom = timeFrom;
        return this;
    }

    public void setTimeFrom(Instant timeFrom) {
        this.timeFrom = timeFrom;
    }

    public Instant getTimeTo() {
        return timeTo;
    }

    public Session timeTo(Instant timeTo) {
        this.timeTo = timeTo;
        return this;
    }

    public void setTimeTo(Instant timeTo) {
        this.timeTo = timeTo;
    }

    public String getNotes() {
        return notes;
    }

    public Session notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Patient getPatient() {
        return patient;
    }

    public Session patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public User getUser() {
        return user;
    }

    public Session user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Offers getOffer() {
        return offer;
    }

    public Session offer(Offers offers) {
        this.offer = offers;
        return this;
    }

    public void setOffer(Offers offers) {
        this.offer = offers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Session)) {
            return false;
        }
        return id != null && id.equals(((Session) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Session{" +
            "id=" + getId() +
            ", timeFrom='" + getTimeFrom() + "'" +
            ", timeTo='" + getTimeTo() + "'" +
            ", notes='" + getNotes() + "'" +
            "}";
    }
}
