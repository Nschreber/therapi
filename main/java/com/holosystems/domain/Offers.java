package com.holosystems.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Offers.
 */
@Entity
@Table(name = "offers")
public class Offers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "offer_name")
    private String offerName;

    @Column(name = "offer_description")
    private String offerDescription;

    @Column(name = "avalable_from")
    private LocalDate avalableFrom;

    @Column(name = "avalable_to")
    private LocalDate avalableTo;

    @ManyToMany
    @JoinTable(name = "offers_states",
               joinColumns = @JoinColumn(name = "offers_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "states_id", referencedColumnName = "id"))
    private Set<States> states = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "offers_practicies",
               joinColumns = @JoinColumn(name = "offers_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "practicies_id", referencedColumnName = "id"))
    private Set<Practicies> practicies = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOfferName() {
        return offerName;
    }

    public Offers offerName(String offerName) {
        this.offerName = offerName;
        return this;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public Offers offerDescription(String offerDescription) {
        this.offerDescription = offerDescription;
        return this;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public LocalDate getAvalableFrom() {
        return avalableFrom;
    }

    public Offers avalableFrom(LocalDate avalableFrom) {
        this.avalableFrom = avalableFrom;
        return this;
    }

    public void setAvalableFrom(LocalDate avalableFrom) {
        this.avalableFrom = avalableFrom;
    }

    public LocalDate getAvalableTo() {
        return avalableTo;
    }

    public Offers avalableTo(LocalDate avalableTo) {
        this.avalableTo = avalableTo;
        return this;
    }

    public void setAvalableTo(LocalDate avalableTo) {
        this.avalableTo = avalableTo;
    }

    public Set<States> getStates() {
        return states;
    }

    public Offers states(Set<States> states) {
        this.states = states;
        return this;
    }

    public Offers addStates(States states) {
        this.states.add(states);
        states.getOffers().add(this);
        return this;
    }

    public Offers removeStates(States states) {
        this.states.remove(states);
        states.getOffers().remove(this);
        return this;
    }

    public void setStates(Set<States> states) {
        this.states = states;
    }

    public Set<Practicies> getPracticies() {
        return practicies;
    }

    public Offers practicies(Set<Practicies> practicies) {
        this.practicies = practicies;
        return this;
    }

    public Offers addPracticies(Practicies practicies) {
        this.practicies.add(practicies);
        practicies.getOffers().add(this);
        return this;
    }

    public Offers removePracticies(Practicies practicies) {
        this.practicies.remove(practicies);
        practicies.getOffers().remove(this);
        return this;
    }

    public void setPracticies(Set<Practicies> practicies) {
        this.practicies = practicies;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Offers)) {
            return false;
        }
        return id != null && id.equals(((Offers) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Offers{" +
            "id=" + getId() +
            ", offerName='" + getOfferName() + "'" +
            ", offerDescription='" + getOfferDescription() + "'" +
            ", avalableFrom='" + getAvalableFrom() + "'" +
            ", avalableTo='" + getAvalableTo() + "'" +
            "}";
    }
}
