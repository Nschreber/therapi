package com.holosystems.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.LocalDate;

/**
 * A Perscription.
 */
@Entity
@Table(name = "perscription")
public class Perscription implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "recipie_class")
    private String recipieClass;

    @Column(name = "recipie_date")
    private LocalDate recipieDate;

    @Column(name = "latest_possible_begin")
    private LocalDate latestPossibleBegin;

    @Column(name = "type_of_regulation")
    private String typeOfRegulation;

    @Column(name = "perscribed_remedies_1")
    private String perscribedRemedies1;

    @Column(name = "perscribed_remedies_2")
    private String perscribedRemedies2;

    @Column(name = "perscribed_remedies_3")
    private String perscribedRemedies3;

    @Column(name = "perscribed_remedies_4")
    private String perscribedRemedies4;

    @Column(name = "perscribed_remedies_count_1")
    private String perscribedRemediesCount1;

    @Column(name = "perscribed_remedies_count_2")
    private String perscribedRemediesCount2;

    @Column(name = "perscribed_remedies_count_3")
    private String perscribedRemediesCount3;

    @Column(name = "perscribed_remedies_count_4")
    private String perscribedRemediesCount4;

    @Column(name = "treatment_frequency")
    private String treatmentFrequency;

    @Column(name = "duratation_treatment")
    private Integer duratationTreatment;

    @Column(name = "indication_key")
    private String indicationKey;

    @Column(name = "barcode_format")
    private String barcodeFormat;

    @Column(name = "colorcode_tk")
    private String colorcodeTK;

    @Column(name = "icd_10_code_1")
    private String icd10Code1;

    @Column(name = "icd_10_code_2")
    private String icd10Code2;

    @Column(name = "diagnosis_from_doctor")
    private String diagnosisFromDoctor;

    @Column(name = "home_visit")
    private Boolean homeVisit;

    @Column(name = "report")
    private Boolean report;

    @ManyToOne
    @JsonIgnoreProperties("perscriptions")
    private Patient patient;

    @ManyToOne
    @JsonIgnoreProperties("perscriptions")
    private Doctor perscribingDoctor;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecipieClass() {
        return recipieClass;
    }

    public Perscription recipieClass(String recipieClass) {
        this.recipieClass = recipieClass;
        return this;
    }

    public void setRecipieClass(String recipieClass) {
        this.recipieClass = recipieClass;
    }

    public LocalDate getRecipieDate() {
        return recipieDate;
    }

    public Perscription recipieDate(LocalDate recipieDate) {
        this.recipieDate = recipieDate;
        return this;
    }

    public void setRecipieDate(LocalDate recipieDate) {
        this.recipieDate = recipieDate;
    }

    public LocalDate getLatestPossibleBegin() {
        return latestPossibleBegin;
    }

    public Perscription latestPossibleBegin(LocalDate latestPossibleBegin) {
        this.latestPossibleBegin = latestPossibleBegin;
        return this;
    }

    public void setLatestPossibleBegin(LocalDate latestPossibleBegin) {
        this.latestPossibleBegin = latestPossibleBegin;
    }

    public String getTypeOfRegulation() {
        return typeOfRegulation;
    }

    public Perscription typeOfRegulation(String typeOfRegulation) {
        this.typeOfRegulation = typeOfRegulation;
        return this;
    }

    public void setTypeOfRegulation(String typeOfRegulation) {
        this.typeOfRegulation = typeOfRegulation;
    }

    public String getPerscribedRemedies1() {
        return perscribedRemedies1;
    }

    public Perscription perscribedRemedies1(String perscribedRemedies1) {
        this.perscribedRemedies1 = perscribedRemedies1;
        return this;
    }

    public void setPerscribedRemedies1(String perscribedRemedies1) {
        this.perscribedRemedies1 = perscribedRemedies1;
    }

    public String getPerscribedRemedies2() {
        return perscribedRemedies2;
    }

    public Perscription perscribedRemedies2(String perscribedRemedies2) {
        this.perscribedRemedies2 = perscribedRemedies2;
        return this;
    }

    public void setPerscribedRemedies2(String perscribedRemedies2) {
        this.perscribedRemedies2 = perscribedRemedies2;
    }

    public String getPerscribedRemedies3() {
        return perscribedRemedies3;
    }

    public Perscription perscribedRemedies3(String perscribedRemedies3) {
        this.perscribedRemedies3 = perscribedRemedies3;
        return this;
    }

    public void setPerscribedRemedies3(String perscribedRemedies3) {
        this.perscribedRemedies3 = perscribedRemedies3;
    }

    public String getPerscribedRemedies4() {
        return perscribedRemedies4;
    }

    public Perscription perscribedRemedies4(String perscribedRemedies4) {
        this.perscribedRemedies4 = perscribedRemedies4;
        return this;
    }

    public void setPerscribedRemedies4(String perscribedRemedies4) {
        this.perscribedRemedies4 = perscribedRemedies4;
    }

    public String getPerscribedRemediesCount1() {
        return perscribedRemediesCount1;
    }

    public Perscription perscribedRemediesCount1(String perscribedRemediesCount1) {
        this.perscribedRemediesCount1 = perscribedRemediesCount1;
        return this;
    }

    public void setPerscribedRemediesCount1(String perscribedRemediesCount1) {
        this.perscribedRemediesCount1 = perscribedRemediesCount1;
    }

    public String getPerscribedRemediesCount2() {
        return perscribedRemediesCount2;
    }

    public Perscription perscribedRemediesCount2(String perscribedRemediesCount2) {
        this.perscribedRemediesCount2 = perscribedRemediesCount2;
        return this;
    }

    public void setPerscribedRemediesCount2(String perscribedRemediesCount2) {
        this.perscribedRemediesCount2 = perscribedRemediesCount2;
    }

    public String getPerscribedRemediesCount3() {
        return perscribedRemediesCount3;
    }

    public Perscription perscribedRemediesCount3(String perscribedRemediesCount3) {
        this.perscribedRemediesCount3 = perscribedRemediesCount3;
        return this;
    }

    public void setPerscribedRemediesCount3(String perscribedRemediesCount3) {
        this.perscribedRemediesCount3 = perscribedRemediesCount3;
    }

    public String getPerscribedRemediesCount4() {
        return perscribedRemediesCount4;
    }

    public Perscription perscribedRemediesCount4(String perscribedRemediesCount4) {
        this.perscribedRemediesCount4 = perscribedRemediesCount4;
        return this;
    }

    public void setPerscribedRemediesCount4(String perscribedRemediesCount4) {
        this.perscribedRemediesCount4 = perscribedRemediesCount4;
    }

    public String getTreatmentFrequency() {
        return treatmentFrequency;
    }

    public Perscription treatmentFrequency(String treatmentFrequency) {
        this.treatmentFrequency = treatmentFrequency;
        return this;
    }

    public void setTreatmentFrequency(String treatmentFrequency) {
        this.treatmentFrequency = treatmentFrequency;
    }

    public Integer getDuratationTreatment() {
        return duratationTreatment;
    }

    public Perscription duratationTreatment(Integer duratationTreatment) {
        this.duratationTreatment = duratationTreatment;
        return this;
    }

    public void setDuratationTreatment(Integer duratationTreatment) {
        this.duratationTreatment = duratationTreatment;
    }

    public String getIndicationKey() {
        return indicationKey;
    }

    public Perscription indicationKey(String indicationKey) {
        this.indicationKey = indicationKey;
        return this;
    }

    public void setIndicationKey(String indicationKey) {
        this.indicationKey = indicationKey;
    }

    public String getBarcodeFormat() {
        return barcodeFormat;
    }

    public Perscription barcodeFormat(String barcodeFormat) {
        this.barcodeFormat = barcodeFormat;
        return this;
    }

    public void setBarcodeFormat(String barcodeFormat) {
        this.barcodeFormat = barcodeFormat;
    }

    public String getColorcodeTK() {
        return colorcodeTK;
    }

    public Perscription colorcodeTK(String colorcodeTK) {
        this.colorcodeTK = colorcodeTK;
        return this;
    }

    public void setColorcodeTK(String colorcodeTK) {
        this.colorcodeTK = colorcodeTK;
    }

    public String getIcd10Code1() {
        return icd10Code1;
    }

    public Perscription icd10Code1(String icd10Code1) {
        this.icd10Code1 = icd10Code1;
        return this;
    }

    public void setIcd10Code1(String icd10Code1) {
        this.icd10Code1 = icd10Code1;
    }

    public String getIcd10Code2() {
        return icd10Code2;
    }

    public Perscription icd10Code2(String icd10Code2) {
        this.icd10Code2 = icd10Code2;
        return this;
    }

    public void setIcd10Code2(String icd10Code2) {
        this.icd10Code2 = icd10Code2;
    }

    public String getDiagnosisFromDoctor() {
        return diagnosisFromDoctor;
    }

    public Perscription diagnosisFromDoctor(String diagnosisFromDoctor) {
        this.diagnosisFromDoctor = diagnosisFromDoctor;
        return this;
    }

    public void setDiagnosisFromDoctor(String diagnosisFromDoctor) {
        this.diagnosisFromDoctor = diagnosisFromDoctor;
    }

    public Boolean isHomeVisit() {
        return homeVisit;
    }

    public Perscription homeVisit(Boolean homeVisit) {
        this.homeVisit = homeVisit;
        return this;
    }

    public void setHomeVisit(Boolean homeVisit) {
        this.homeVisit = homeVisit;
    }

    public Boolean isReport() {
        return report;
    }

    public Perscription report(Boolean report) {
        this.report = report;
        return this;
    }

    public void setReport(Boolean report) {
        this.report = report;
    }

    public Patient getPatient() {
        return patient;
    }

    public Perscription patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getPerscribingDoctor() {
        return perscribingDoctor;
    }

    public Perscription perscribingDoctor(Doctor doctor) {
        this.perscribingDoctor = doctor;
        return this;
    }

    public void setPerscribingDoctor(Doctor doctor) {
        this.perscribingDoctor = doctor;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Perscription)) {
            return false;
        }
        return id != null && id.equals(((Perscription) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Perscription{" +
            "id=" + getId() +
            ", recipieClass='" + getRecipieClass() + "'" +
            ", recipieDate='" + getRecipieDate() + "'" +
            ", latestPossibleBegin='" + getLatestPossibleBegin() + "'" +
            ", typeOfRegulation='" + getTypeOfRegulation() + "'" +
            ", perscribedRemedies1='" + getPerscribedRemedies1() + "'" +
            ", perscribedRemedies2='" + getPerscribedRemedies2() + "'" +
            ", perscribedRemedies3='" + getPerscribedRemedies3() + "'" +
            ", perscribedRemedies4='" + getPerscribedRemedies4() + "'" +
            ", perscribedRemediesCount1='" + getPerscribedRemediesCount1() + "'" +
            ", perscribedRemediesCount2='" + getPerscribedRemediesCount2() + "'" +
            ", perscribedRemediesCount3='" + getPerscribedRemediesCount3() + "'" +
            ", perscribedRemediesCount4='" + getPerscribedRemediesCount4() + "'" +
            ", treatmentFrequency='" + getTreatmentFrequency() + "'" +
            ", duratationTreatment=" + getDuratationTreatment() +
            ", indicationKey='" + getIndicationKey() + "'" +
            ", barcodeFormat='" + getBarcodeFormat() + "'" +
            ", colorcodeTK='" + getColorcodeTK() + "'" +
            ", icd10Code1='" + getIcd10Code1() + "'" +
            ", icd10Code2='" + getIcd10Code2() + "'" +
            ", diagnosisFromDoctor='" + getDiagnosisFromDoctor() + "'" +
            ", homeVisit='" + isHomeVisit() + "'" +
            ", report='" + isReport() + "'" +
            "}";
    }
}
