package com.holosystems.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.LocalDate;

/**
 * A Patient.
 */
@Entity
@Table(name = "patient")
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private Integer phoneNumber;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "address_country")
    private String addressCountry;

    @Column(name = "address_state")
    private String addressState;

    @Column(name = "address_postal_code")
    private String addressPostalCode;

    @Column(name = "address_street")
    private String addressStreet;

    @Column(name = "address_house_number")
    private String addressHouseNumber;

    @Column(name = "address_additive")
    private String addressAdditive;

    @Column(name = "health_insurance")
    private String healthInsurance;

    @Column(name = "birthday")
    private LocalDate birthday;

    @ManyToOne
    @JsonIgnoreProperties("patients")
    private Doctor defaultDoctor;

    @ManyToOne
    @JsonIgnoreProperties("patients")
    private User defaultUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Patient firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Patient lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public Patient phoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public Patient emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public Patient addressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
        return this;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    public String getAddressState() {
        return addressState;
    }

    public Patient addressState(String addressState) {
        this.addressState = addressState;
        return this;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public String getAddressPostalCode() {
        return addressPostalCode;
    }

    public Patient addressPostalCode(String addressPostalCode) {
        this.addressPostalCode = addressPostalCode;
        return this;
    }

    public void setAddressPostalCode(String addressPostalCode) {
        this.addressPostalCode = addressPostalCode;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public Patient addressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
        return this;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressHouseNumber() {
        return addressHouseNumber;
    }

    public Patient addressHouseNumber(String addressHouseNumber) {
        this.addressHouseNumber = addressHouseNumber;
        return this;
    }

    public void setAddressHouseNumber(String addressHouseNumber) {
        this.addressHouseNumber = addressHouseNumber;
    }

    public String getAddressAdditive() {
        return addressAdditive;
    }

    public Patient addressAdditive(String addressAdditive) {
        this.addressAdditive = addressAdditive;
        return this;
    }

    public void setAddressAdditive(String addressAdditive) {
        this.addressAdditive = addressAdditive;
    }

    public String getHealthInsurance() {
        return healthInsurance;
    }

    public Patient healthInsurance(String healthInsurance) {
        this.healthInsurance = healthInsurance;
        return this;
    }

    public void setHealthInsurance(String healthInsurance) {
        this.healthInsurance = healthInsurance;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public Patient birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Doctor getDefaultDoctor() {
        return defaultDoctor;
    }

    public Patient defaultDoctor(Doctor doctor) {
        this.defaultDoctor = doctor;
        return this;
    }

    public void setDefaultDoctor(Doctor doctor) {
        this.defaultDoctor = doctor;
    }

    public User getDefaultUser() {
        return defaultUser;
    }

    public Patient defaultUser(User user) {
        this.defaultUser = user;
        return this;
    }

    public void setDefaultUser(User user) {
        this.defaultUser = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Patient)) {
            return false;
        }
        return id != null && id.equals(((Patient) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Patient{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", phoneNumber=" + getPhoneNumber() +
            ", emailAddress='" + getEmailAddress() + "'" +
            ", addressCountry='" + getAddressCountry() + "'" +
            ", addressState='" + getAddressState() + "'" +
            ", addressPostalCode='" + getAddressPostalCode() + "'" +
            ", addressStreet='" + getAddressStreet() + "'" +
            ", addressHouseNumber='" + getAddressHouseNumber() + "'" +
            ", addressAdditive='" + getAddressAdditive() + "'" +
            ", healthInsurance='" + getHealthInsurance() + "'" +
            ", birthday='" + getBirthday() + "'" +
            "}";
    }
}
