import { Moment } from 'moment';
import { IStates } from 'app/shared/model/states.model';
import { IPracticies } from 'app/shared/model/practicies.model';

export interface IOffers {
  id?: number;
  offerName?: string;
  offerDescription?: string;
  avalableFrom?: Moment;
  avalableTo?: Moment;
  states?: IStates[];
  practicies?: IPracticies[];
}

export class Offers implements IOffers {
  constructor(
    public id?: number,
    public offerName?: string,
    public offerDescription?: string,
    public avalableFrom?: Moment,
    public avalableTo?: Moment,
    public states?: IStates[],
    public practicies?: IPracticies[]
  ) {}
}
