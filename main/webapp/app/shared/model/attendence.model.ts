import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IAttendence {
  id?: number;
  timeFrom?: Moment;
  timeTo?: Moment;
  user?: IUser;
}

export class Attendence implements IAttendence {
  constructor(public id?: number, public timeFrom?: Moment, public timeTo?: Moment, public user?: IUser) {}
}
