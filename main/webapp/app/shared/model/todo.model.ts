import { IUser } from 'app/core/user/user.model';

export interface ITodo {
  id?: number;
  title?: string;
  text?: string;
  shared?: boolean;
  user?: IUser;
}

export class Todo implements ITodo {
  constructor(public id?: number, public title?: string, public text?: string, public shared?: boolean, public user?: IUser) {
    this.shared = this.shared || false;
  }
}
