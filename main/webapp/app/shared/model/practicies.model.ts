import { IOffers } from 'app/shared/model/offers.model';

export interface IPracticies {
  id?: number;
  name?: string;
  institutionalFlag?: number;
  offers?: IOffers[];
}

export class Practicies implements IPracticies {
  constructor(public id?: number, public name?: string, public institutionalFlag?: number, public offers?: IOffers[]) {}
}
