import { IOffers } from 'app/shared/model/offers.model';

export interface IStates {
  id?: number;
  stateName?: string;
  offers?: IOffers[];
}

export class States implements IStates {
  constructor(public id?: number, public stateName?: string, public offers?: IOffers[]) {}
}
