import { Moment } from 'moment';
import { IPatient } from 'app/shared/model/patient.model';
import { IUser } from 'app/core/user/user.model';

export interface IAppointment {
  id?: number;
  time?: Moment;
  patient?: IPatient;
  user?: IUser;
}

export class Appointment implements IAppointment {
  constructor(public id?: number, public time?: Moment, public patient?: IPatient, public user?: IUser) {}
}
