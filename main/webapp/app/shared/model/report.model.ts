import { IUser } from 'app/core/user/user.model';
import { IPatient } from 'app/shared/model/patient.model';
import { IDoctor } from 'app/shared/model/doctor.model';
import { IPerscription } from 'app/shared/model/perscription.model';

export interface IReport {
  id?: number;
  title?: string;
  text?: string;
  isDrafted?: boolean;
  author?: IUser;
  patient?: IPatient;
  doctor?: IDoctor;
  recipie?: IPerscription;
}

export class Report implements IReport {
  constructor(
    public id?: number,
    public title?: string,
    public text?: string,
    public isDrafted?: boolean,
    public author?: IUser,
    public patient?: IPatient,
    public doctor?: IDoctor,
    public recipie?: IPerscription
  ) {
    this.isDrafted = this.isDrafted || false;
  }
}
