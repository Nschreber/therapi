export interface IDoctor {
  id?: number;
  title?: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  emailAddress?: string;
  addressCountry?: string;
  addressState?: string;
  addressPostalCode?: string;
  addressStreet?: string;
  addressHouseNumber?: string;
  addressAdditive?: string;
}

export class Doctor implements IDoctor {
  constructor(
    public id?: number,
    public title?: string,
    public firstName?: string,
    public lastName?: string,
    public phoneNumber?: string,
    public emailAddress?: string,
    public addressCountry?: string,
    public addressState?: string,
    public addressPostalCode?: string,
    public addressStreet?: string,
    public addressHouseNumber?: string,
    public addressAdditive?: string
  ) {}
}
