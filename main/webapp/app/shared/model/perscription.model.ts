import { Moment } from 'moment';
import { IPatient } from 'app/shared/model/patient.model';
import { IDoctor } from 'app/shared/model/doctor.model';

export interface IPerscription {
  id?: number;
  recipieClass?: string;
  recipieDate?: Moment;
  latestPossibleBegin?: Moment;
  typeOfRegulation?: string;
  perscribedRemedies1?: string;
  perscribedRemedies2?: string;
  perscribedRemedies3?: string;
  perscribedRemedies4?: string;
  perscribedRemediesCount1?: string;
  perscribedRemediesCount2?: string;
  perscribedRemediesCount3?: string;
  perscribedRemediesCount4?: string;
  treatmentFrequency?: string;
  duratationTreatment?: number;
  indicationKey?: string;
  barcodeFormat?: string;
  colorcodeTK?: string;
  icd10Code1?: string;
  icd10Code2?: string;
  diagnosisFromDoctor?: string;
  homeVisit?: boolean;
  report?: boolean;
  patient?: IPatient;
  perscribingDoctor?: IDoctor;
}

export class Perscription implements IPerscription {
  constructor(
    public id?: number,
    public recipieClass?: string,
    public recipieDate?: Moment,
    public latestPossibleBegin?: Moment,
    public typeOfRegulation?: string,
    public perscribedRemedies1?: string,
    public perscribedRemedies2?: string,
    public perscribedRemedies3?: string,
    public perscribedRemedies4?: string,
    public perscribedRemediesCount1?: string,
    public perscribedRemediesCount2?: string,
    public perscribedRemediesCount3?: string,
    public perscribedRemediesCount4?: string,
    public treatmentFrequency?: string,
    public duratationTreatment?: number,
    public indicationKey?: string,
    public barcodeFormat?: string,
    public colorcodeTK?: string,
    public icd10Code1?: string,
    public icd10Code2?: string,
    public diagnosisFromDoctor?: string,
    public homeVisit?: boolean,
    public report?: boolean,
    public patient?: IPatient,
    public perscribingDoctor?: IDoctor
  ) {
    this.homeVisit = this.homeVisit || false;
    this.report = this.report || false;
  }
}
