import { Moment } from 'moment';
import { IPatient } from 'app/shared/model/patient.model';
import { IUser } from 'app/core/user/user.model';
import { IOffers } from 'app/shared/model/offers.model';

export interface ISession {
  id?: number;
  timeFrom?: Moment;
  timeTo?: Moment;
  notes?: string;
  patient?: IPatient;
  user?: IUser;
  offer?: IOffers;
}

export class Session implements ISession {
  constructor(
    public id?: number,
    public timeFrom?: Moment,
    public timeTo?: Moment,
    public notes?: string,
    public patient?: IPatient,
    public user?: IUser,
    public offer?: IOffers
  ) {}
}
