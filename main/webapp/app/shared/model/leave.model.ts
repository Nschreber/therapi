import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface ILeave {
  id?: number;
  leaveFrom?: Moment;
  leaveTo?: Moment;
  reason?: string;
  user?: IUser;
}

export class Leave implements ILeave {
  constructor(public id?: number, public leaveFrom?: Moment, public leaveTo?: Moment, public reason?: string, public user?: IUser) {}
}
