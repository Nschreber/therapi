import { Moment } from 'moment';
import { IDoctor } from 'app/shared/model/doctor.model';
import { IUser } from 'app/core/user/user.model';

export interface IPatient {
  id?: number;
  firstName?: string;
  lastName?: string;
  phoneNumber?: number;
  emailAddress?: string;
  addressCountry?: string;
  addressState?: string;
  addressPostalCode?: string;
  addressStreet?: string;
  addressHouseNumber?: string;
  addressAdditive?: string;
  healthInsurance?: string;
  birthday?: Moment;
  defaultDoctor?: IDoctor;
  defaultUser?: IUser;
}

export class Patient implements IPatient {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public phoneNumber?: number,
    public emailAddress?: string,
    public addressCountry?: string,
    public addressState?: string,
    public addressPostalCode?: string,
    public addressStreet?: string,
    public addressHouseNumber?: string,
    public addressAdditive?: string,
    public healthInsurance?: string,
    public birthday?: Moment,
    public defaultDoctor?: IDoctor,
    public defaultUser?: IUser
  ) {}
}
