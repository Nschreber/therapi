import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'states',
        loadChildren: () => import('./states/states.module').then(m => m.TheraPiStatesModule)
      },
      {
        path: 'patient',
        loadChildren: () => import('./patient/patient.module').then(m => m.TheraPiPatientModule)
      },
      {
        path: 'practicies',
        loadChildren: () => import('./practicies/practicies.module').then(m => m.TheraPiPracticiesModule)
      },
      {
        path: 'doctor',
        loadChildren: () => import('./doctor/doctor.module').then(m => m.TheraPiDoctorModule)
      },
      {
        path: 'appointment',
        loadChildren: () => import('./appointment/appointment.module').then(m => m.TheraPiAppointmentModule)
      },
      {
        path: 'attendence',
        loadChildren: () => import('./attendence/attendence.module').then(m => m.TheraPiAttendenceModule)
      },
      {
        path: 'offers',
        loadChildren: () => import('./offers/offers.module').then(m => m.TheraPiOffersModule)
      },
      {
        path: 'leave',
        loadChildren: () => import('./leave/leave.module').then(m => m.TheraPiLeaveModule)
      },
      {
        path: 'perscription',
        loadChildren: () => import('./perscription/perscription.module').then(m => m.TheraPiPerscriptionModule)
      },
      {
        path: 'report',
        loadChildren: () => import('./report/report.module').then(m => m.TheraPiReportModule)
      },
      {
        path: 'session',
        loadChildren: () => import('./session/session.module').then(m => m.TheraPiSessionModule)
      },
      {
        path: 'todo',
        loadChildren: () => import('./todo/todo.module').then(m => m.TheraPiTodoModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class TheraPiEntityModule {}
