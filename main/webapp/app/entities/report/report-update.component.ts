import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IReport, Report } from 'app/shared/model/report.model';
import { ReportService } from './report.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IPatient } from 'app/shared/model/patient.model';
import { PatientService } from 'app/entities/patient/patient.service';
import { IDoctor } from 'app/shared/model/doctor.model';
import { DoctorService } from 'app/entities/doctor/doctor.service';
import { IPerscription } from 'app/shared/model/perscription.model';
import { PerscriptionService } from 'app/entities/perscription/perscription.service';

type SelectableEntity = IUser | IPatient | IDoctor | IPerscription;

@Component({
  selector: 'jhi-report-update',
  templateUrl: './report-update.component.html'
})
export class ReportUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  patients: IPatient[] = [];
  doctors: IDoctor[] = [];
  perscriptions: IPerscription[] = [];

  editForm = this.fb.group({
    id: [],
    title: [],
    text: [],
    isDrafted: [],
    author: [],
    patient: [],
    doctor: [],
    recipie: []
  });

  constructor(
    protected reportService: ReportService,
    protected userService: UserService,
    protected patientService: PatientService,
    protected doctorService: DoctorService,
    protected perscriptionService: PerscriptionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ report }) => {
      this.updateForm(report);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.patientService.query().subscribe((res: HttpResponse<IPatient[]>) => (this.patients = res.body || []));

      this.doctorService.query().subscribe((res: HttpResponse<IDoctor[]>) => (this.doctors = res.body || []));

      this.perscriptionService.query().subscribe((res: HttpResponse<IPerscription[]>) => (this.perscriptions = res.body || []));
    });
  }

  updateForm(report: IReport): void {
    this.editForm.patchValue({
      id: report.id,
      title: report.title,
      text: report.text,
      isDrafted: report.isDrafted,
      author: report.author,
      patient: report.patient,
      doctor: report.doctor,
      recipie: report.recipie
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const report = this.createFromForm();
    if (report.id !== undefined) {
      this.subscribeToSaveResponse(this.reportService.update(report));
    } else {
      this.subscribeToSaveResponse(this.reportService.create(report));
    }
  }

  private createFromForm(): IReport {
    return {
      ...new Report(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      text: this.editForm.get(['text'])!.value,
      isDrafted: this.editForm.get(['isDrafted'])!.value,
      author: this.editForm.get(['author'])!.value,
      patient: this.editForm.get(['patient'])!.value,
      doctor: this.editForm.get(['doctor'])!.value,
      recipie: this.editForm.get(['recipie'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReport>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
