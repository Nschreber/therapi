import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPerscription } from 'app/shared/model/perscription.model';

type EntityResponseType = HttpResponse<IPerscription>;
type EntityArrayResponseType = HttpResponse<IPerscription[]>;

@Injectable({ providedIn: 'root' })
export class PerscriptionService {
  public resourceUrl = SERVER_API_URL + 'api/perscriptions';

  constructor(protected http: HttpClient) {}

  create(perscription: IPerscription): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(perscription);
    return this.http
      .post<IPerscription>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(perscription: IPerscription): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(perscription);
    return this.http
      .put<IPerscription>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPerscription>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPerscription[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(perscription: IPerscription): IPerscription {
    const copy: IPerscription = Object.assign({}, perscription, {
      recipieDate:
        perscription.recipieDate && perscription.recipieDate.isValid() ? perscription.recipieDate.format(DATE_FORMAT) : undefined,
      latestPossibleBegin:
        perscription.latestPossibleBegin && perscription.latestPossibleBegin.isValid()
          ? perscription.latestPossibleBegin.format(DATE_FORMAT)
          : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.recipieDate = res.body.recipieDate ? moment(res.body.recipieDate) : undefined;
      res.body.latestPossibleBegin = res.body.latestPossibleBegin ? moment(res.body.latestPossibleBegin) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((perscription: IPerscription) => {
        perscription.recipieDate = perscription.recipieDate ? moment(perscription.recipieDate) : undefined;
        perscription.latestPossibleBegin = perscription.latestPossibleBegin ? moment(perscription.latestPossibleBegin) : undefined;
      });
    }
    return res;
  }
}
