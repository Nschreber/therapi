import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPerscription } from 'app/shared/model/perscription.model';

@Component({
  selector: 'jhi-perscription-detail',
  templateUrl: './perscription-detail.component.html'
})
export class PerscriptionDetailComponent implements OnInit {
  perscription: IPerscription | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ perscription }) => (this.perscription = perscription));
  }

  previousState(): void {
    window.history.back();
  }
}
