import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPerscription } from 'app/shared/model/perscription.model';
import { PerscriptionService } from './perscription.service';

@Component({
  templateUrl: './perscription-delete-dialog.component.html'
})
export class PerscriptionDeleteDialogComponent {
  perscription?: IPerscription;

  constructor(
    protected perscriptionService: PerscriptionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.perscriptionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('perscriptionListModification');
      this.activeModal.close();
    });
  }
}
