import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPerscription } from 'app/shared/model/perscription.model';
import { PerscriptionService } from './perscription.service';
import { PerscriptionDeleteDialogComponent } from './perscription-delete-dialog.component';

@Component({
  selector: 'jhi-perscription',
  templateUrl: './perscription.component.html'
})
export class PerscriptionComponent implements OnInit, OnDestroy {
  perscriptions?: IPerscription[];
  eventSubscriber?: Subscription;

  constructor(
    protected perscriptionService: PerscriptionService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.perscriptionService.query().subscribe((res: HttpResponse<IPerscription[]>) => (this.perscriptions = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPerscriptions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPerscription): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPerscriptions(): void {
    this.eventSubscriber = this.eventManager.subscribe('perscriptionListModification', () => this.loadAll());
  }

  delete(perscription: IPerscription): void {
    const modalRef = this.modalService.open(PerscriptionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.perscription = perscription;
  }
}
