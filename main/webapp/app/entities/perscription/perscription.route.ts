import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPerscription, Perscription } from 'app/shared/model/perscription.model';
import { PerscriptionService } from './perscription.service';
import { PerscriptionComponent } from './perscription.component';
import { PerscriptionDetailComponent } from './perscription-detail.component';
import { PerscriptionUpdateComponent } from './perscription-update.component';

@Injectable({ providedIn: 'root' })
export class PerscriptionResolve implements Resolve<IPerscription> {
  constructor(private service: PerscriptionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPerscription> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((perscription: HttpResponse<Perscription>) => {
          if (perscription.body) {
            return of(perscription.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Perscription());
  }
}

export const perscriptionRoute: Routes = [
  {
    path: '',
    component: PerscriptionComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.perscription.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PerscriptionDetailComponent,
    resolve: {
      perscription: PerscriptionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.perscription.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PerscriptionUpdateComponent,
    resolve: {
      perscription: PerscriptionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.perscription.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PerscriptionUpdateComponent,
    resolve: {
      perscription: PerscriptionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.perscription.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
