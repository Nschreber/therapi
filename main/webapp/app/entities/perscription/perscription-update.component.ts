import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPerscription, Perscription } from 'app/shared/model/perscription.model';
import { PerscriptionService } from './perscription.service';
import { IPatient } from 'app/shared/model/patient.model';
import { PatientService } from 'app/entities/patient/patient.service';
import { IDoctor } from 'app/shared/model/doctor.model';
import { DoctorService } from 'app/entities/doctor/doctor.service';

type SelectableEntity = IPatient | IDoctor;

@Component({
  selector: 'jhi-perscription-update',
  templateUrl: './perscription-update.component.html'
})
export class PerscriptionUpdateComponent implements OnInit {
  isSaving = false;
  patients: IPatient[] = [];
  doctors: IDoctor[] = [];
  recipieDateDp: any;
  latestPossibleBeginDp: any;

  editForm = this.fb.group({
    id: [],
    recipieClass: [],
    recipieDate: [],
    latestPossibleBegin: [],
    typeOfRegulation: [],
    perscribedRemedies1: [],
    perscribedRemedies2: [],
    perscribedRemedies3: [],
    perscribedRemedies4: [],
    perscribedRemediesCount1: [],
    perscribedRemediesCount2: [],
    perscribedRemediesCount3: [],
    perscribedRemediesCount4: [],
    treatmentFrequency: [],
    duratationTreatment: [],
    indicationKey: [],
    barcodeFormat: [],
    colorcodeTK: [],
    icd10Code1: [],
    icd10Code2: [],
    diagnosisFromDoctor: [],
    homeVisit: [],
    report: [],
    patient: [],
    perscribingDoctor: []
  });

  constructor(
    protected perscriptionService: PerscriptionService,
    protected patientService: PatientService,
    protected doctorService: DoctorService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ perscription }) => {
      this.updateForm(perscription);

      this.patientService.query().subscribe((res: HttpResponse<IPatient[]>) => (this.patients = res.body || []));

      this.doctorService.query().subscribe((res: HttpResponse<IDoctor[]>) => (this.doctors = res.body || []));
    });
  }

  updateForm(perscription: IPerscription): void {
    this.editForm.patchValue({
      id: perscription.id,
      recipieClass: perscription.recipieClass,
      recipieDate: perscription.recipieDate,
      latestPossibleBegin: perscription.latestPossibleBegin,
      typeOfRegulation: perscription.typeOfRegulation,
      perscribedRemedies1: perscription.perscribedRemedies1,
      perscribedRemedies2: perscription.perscribedRemedies2,
      perscribedRemedies3: perscription.perscribedRemedies3,
      perscribedRemedies4: perscription.perscribedRemedies4,
      perscribedRemediesCount1: perscription.perscribedRemediesCount1,
      perscribedRemediesCount2: perscription.perscribedRemediesCount2,
      perscribedRemediesCount3: perscription.perscribedRemediesCount3,
      perscribedRemediesCount4: perscription.perscribedRemediesCount4,
      treatmentFrequency: perscription.treatmentFrequency,
      duratationTreatment: perscription.duratationTreatment,
      indicationKey: perscription.indicationKey,
      barcodeFormat: perscription.barcodeFormat,
      colorcodeTK: perscription.colorcodeTK,
      icd10Code1: perscription.icd10Code1,
      icd10Code2: perscription.icd10Code2,
      diagnosisFromDoctor: perscription.diagnosisFromDoctor,
      homeVisit: perscription.homeVisit,
      report: perscription.report,
      patient: perscription.patient,
      perscribingDoctor: perscription.perscribingDoctor
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const perscription = this.createFromForm();
    if (perscription.id !== undefined) {
      this.subscribeToSaveResponse(this.perscriptionService.update(perscription));
    } else {
      this.subscribeToSaveResponse(this.perscriptionService.create(perscription));
    }
  }

  private createFromForm(): IPerscription {
    return {
      ...new Perscription(),
      id: this.editForm.get(['id'])!.value,
      recipieClass: this.editForm.get(['recipieClass'])!.value,
      recipieDate: this.editForm.get(['recipieDate'])!.value,
      latestPossibleBegin: this.editForm.get(['latestPossibleBegin'])!.value,
      typeOfRegulation: this.editForm.get(['typeOfRegulation'])!.value,
      perscribedRemedies1: this.editForm.get(['perscribedRemedies1'])!.value,
      perscribedRemedies2: this.editForm.get(['perscribedRemedies2'])!.value,
      perscribedRemedies3: this.editForm.get(['perscribedRemedies3'])!.value,
      perscribedRemedies4: this.editForm.get(['perscribedRemedies4'])!.value,
      perscribedRemediesCount1: this.editForm.get(['perscribedRemediesCount1'])!.value,
      perscribedRemediesCount2: this.editForm.get(['perscribedRemediesCount2'])!.value,
      perscribedRemediesCount3: this.editForm.get(['perscribedRemediesCount3'])!.value,
      perscribedRemediesCount4: this.editForm.get(['perscribedRemediesCount4'])!.value,
      treatmentFrequency: this.editForm.get(['treatmentFrequency'])!.value,
      duratationTreatment: this.editForm.get(['duratationTreatment'])!.value,
      indicationKey: this.editForm.get(['indicationKey'])!.value,
      barcodeFormat: this.editForm.get(['barcodeFormat'])!.value,
      colorcodeTK: this.editForm.get(['colorcodeTK'])!.value,
      icd10Code1: this.editForm.get(['icd10Code1'])!.value,
      icd10Code2: this.editForm.get(['icd10Code2'])!.value,
      diagnosisFromDoctor: this.editForm.get(['diagnosisFromDoctor'])!.value,
      homeVisit: this.editForm.get(['homeVisit'])!.value,
      report: this.editForm.get(['report'])!.value,
      patient: this.editForm.get(['patient'])!.value,
      perscribingDoctor: this.editForm.get(['perscribingDoctor'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPerscription>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
