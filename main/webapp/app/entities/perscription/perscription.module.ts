import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TheraPiSharedModule } from 'app/shared/shared.module';
import { PerscriptionComponent } from './perscription.component';
import { PerscriptionDetailComponent } from './perscription-detail.component';
import { PerscriptionUpdateComponent } from './perscription-update.component';
import { PerscriptionDeleteDialogComponent } from './perscription-delete-dialog.component';
import { perscriptionRoute } from './perscription.route';

@NgModule({
  imports: [TheraPiSharedModule, RouterModule.forChild(perscriptionRoute)],
  declarations: [PerscriptionComponent, PerscriptionDetailComponent, PerscriptionUpdateComponent, PerscriptionDeleteDialogComponent],
  entryComponents: [PerscriptionDeleteDialogComponent]
})
export class TheraPiPerscriptionModule {}
