import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPatient, Patient } from 'app/shared/model/patient.model';
import { PatientService } from './patient.service';
import { IDoctor } from 'app/shared/model/doctor.model';
import { DoctorService } from 'app/entities/doctor/doctor.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

type SelectableEntity = IDoctor | IUser;

@Component({
  selector: 'jhi-patient-update',
  templateUrl: './patient-update.component.html'
})
export class PatientUpdateComponent implements OnInit {
  isSaving = false;
  doctors: IDoctor[] = [];
  users: IUser[] = [];
  birthdayDp: any;

  editForm = this.fb.group({
    id: [],
    firstName: [],
    lastName: [],
    phoneNumber: [],
    emailAddress: [],
    addressCountry: [],
    addressState: [],
    addressPostalCode: [],
    addressStreet: [],
    addressHouseNumber: [],
    addressAdditive: [],
    healthInsurance: [],
    birthday: [],
    defaultDoctor: [],
    defaultUser: []
  });

  constructor(
    protected patientService: PatientService,
    protected doctorService: DoctorService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ patient }) => {
      this.updateForm(patient);

      this.doctorService.query().subscribe((res: HttpResponse<IDoctor[]>) => (this.doctors = res.body || []));

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(patient: IPatient): void {
    this.editForm.patchValue({
      id: patient.id,
      firstName: patient.firstName,
      lastName: patient.lastName,
      phoneNumber: patient.phoneNumber,
      emailAddress: patient.emailAddress,
      addressCountry: patient.addressCountry,
      addressState: patient.addressState,
      addressPostalCode: patient.addressPostalCode,
      addressStreet: patient.addressStreet,
      addressHouseNumber: patient.addressHouseNumber,
      addressAdditive: patient.addressAdditive,
      healthInsurance: patient.healthInsurance,
      birthday: patient.birthday,
      defaultDoctor: patient.defaultDoctor,
      defaultUser: patient.defaultUser
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const patient = this.createFromForm();
    if (patient.id !== undefined) {
      this.subscribeToSaveResponse(this.patientService.update(patient));
    } else {
      this.subscribeToSaveResponse(this.patientService.create(patient));
    }
  }

  private createFromForm(): IPatient {
    return {
      ...new Patient(),
      id: this.editForm.get(['id'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      emailAddress: this.editForm.get(['emailAddress'])!.value,
      addressCountry: this.editForm.get(['addressCountry'])!.value,
      addressState: this.editForm.get(['addressState'])!.value,
      addressPostalCode: this.editForm.get(['addressPostalCode'])!.value,
      addressStreet: this.editForm.get(['addressStreet'])!.value,
      addressHouseNumber: this.editForm.get(['addressHouseNumber'])!.value,
      addressAdditive: this.editForm.get(['addressAdditive'])!.value,
      healthInsurance: this.editForm.get(['healthInsurance'])!.value,
      birthday: this.editForm.get(['birthday'])!.value,
      defaultDoctor: this.editForm.get(['defaultDoctor'])!.value,
      defaultUser: this.editForm.get(['defaultUser'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPatient>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
