import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOffers } from 'app/shared/model/offers.model';
import { OffersService } from './offers.service';

@Component({
  templateUrl: './offers-delete-dialog.component.html'
})
export class OffersDeleteDialogComponent {
  offers?: IOffers;

  constructor(protected offersService: OffersService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.offersService.delete(id).subscribe(() => {
      this.eventManager.broadcast('offersListModification');
      this.activeModal.close();
    });
  }
}
