import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TheraPiSharedModule } from 'app/shared/shared.module';
import { OffersComponent } from './offers.component';
import { OffersDetailComponent } from './offers-detail.component';
import { OffersUpdateComponent } from './offers-update.component';
import { OffersDeleteDialogComponent } from './offers-delete-dialog.component';
import { offersRoute } from './offers.route';

@NgModule({
  imports: [TheraPiSharedModule, RouterModule.forChild(offersRoute)],
  declarations: [OffersComponent, OffersDetailComponent, OffersUpdateComponent, OffersDeleteDialogComponent],
  entryComponents: [OffersDeleteDialogComponent]
})
export class TheraPiOffersModule {}
