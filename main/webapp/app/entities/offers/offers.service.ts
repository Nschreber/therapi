import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOffers } from 'app/shared/model/offers.model';

type EntityResponseType = HttpResponse<IOffers>;
type EntityArrayResponseType = HttpResponse<IOffers[]>;

@Injectable({ providedIn: 'root' })
export class OffersService {
  public resourceUrl = SERVER_API_URL + 'api/offers';

  constructor(protected http: HttpClient) {}

  create(offers: IOffers): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(offers);
    return this.http
      .post<IOffers>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(offers: IOffers): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(offers);
    return this.http
      .put<IOffers>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IOffers>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOffers[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(offers: IOffers): IOffers {
    const copy: IOffers = Object.assign({}, offers, {
      avalableFrom: offers.avalableFrom && offers.avalableFrom.isValid() ? offers.avalableFrom.format(DATE_FORMAT) : undefined,
      avalableTo: offers.avalableTo && offers.avalableTo.isValid() ? offers.avalableTo.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.avalableFrom = res.body.avalableFrom ? moment(res.body.avalableFrom) : undefined;
      res.body.avalableTo = res.body.avalableTo ? moment(res.body.avalableTo) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((offers: IOffers) => {
        offers.avalableFrom = offers.avalableFrom ? moment(offers.avalableFrom) : undefined;
        offers.avalableTo = offers.avalableTo ? moment(offers.avalableTo) : undefined;
      });
    }
    return res;
  }
}
