import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOffers, Offers } from 'app/shared/model/offers.model';
import { OffersService } from './offers.service';
import { OffersComponent } from './offers.component';
import { OffersDetailComponent } from './offers-detail.component';
import { OffersUpdateComponent } from './offers-update.component';

@Injectable({ providedIn: 'root' })
export class OffersResolve implements Resolve<IOffers> {
  constructor(private service: OffersService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOffers> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((offers: HttpResponse<Offers>) => {
          if (offers.body) {
            return of(offers.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Offers());
  }
}

export const offersRoute: Routes = [
  {
    path: '',
    component: OffersComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.offers.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OffersDetailComponent,
    resolve: {
      offers: OffersResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.offers.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OffersUpdateComponent,
    resolve: {
      offers: OffersResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.offers.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OffersUpdateComponent,
    resolve: {
      offers: OffersResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.offers.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
