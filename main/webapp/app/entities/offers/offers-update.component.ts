import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOffers, Offers } from 'app/shared/model/offers.model';
import { OffersService } from './offers.service';
import { IStates } from 'app/shared/model/states.model';
import { StatesService } from 'app/entities/states/states.service';
import { IPracticies } from 'app/shared/model/practicies.model';
import { PracticiesService } from 'app/entities/practicies/practicies.service';

type SelectableEntity = IStates | IPracticies;

@Component({
  selector: 'jhi-offers-update',
  templateUrl: './offers-update.component.html'
})
export class OffersUpdateComponent implements OnInit {
  isSaving = false;
  states: IStates[] = [];
  practicies: IPracticies[] = [];
  avalableFromDp: any;
  avalableToDp: any;

  editForm = this.fb.group({
    id: [],
    offerName: [],
    offerDescription: [],
    avalableFrom: [],
    avalableTo: [],
    states: [],
    practicies: []
  });

  constructor(
    protected offersService: OffersService,
    protected statesService: StatesService,
    protected practiciesService: PracticiesService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ offers }) => {
      this.updateForm(offers);

      this.statesService.query().subscribe((res: HttpResponse<IStates[]>) => (this.states = res.body || []));

      this.practiciesService.query().subscribe((res: HttpResponse<IPracticies[]>) => (this.practicies = res.body || []));
    });
  }

  updateForm(offers: IOffers): void {
    this.editForm.patchValue({
      id: offers.id,
      offerName: offers.offerName,
      offerDescription: offers.offerDescription,
      avalableFrom: offers.avalableFrom,
      avalableTo: offers.avalableTo,
      states: offers.states,
      practicies: offers.practicies
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const offers = this.createFromForm();
    if (offers.id !== undefined) {
      this.subscribeToSaveResponse(this.offersService.update(offers));
    } else {
      this.subscribeToSaveResponse(this.offersService.create(offers));
    }
  }

  private createFromForm(): IOffers {
    return {
      ...new Offers(),
      id: this.editForm.get(['id'])!.value,
      offerName: this.editForm.get(['offerName'])!.value,
      offerDescription: this.editForm.get(['offerDescription'])!.value,
      avalableFrom: this.editForm.get(['avalableFrom'])!.value,
      avalableTo: this.editForm.get(['avalableTo'])!.value,
      states: this.editForm.get(['states'])!.value,
      practicies: this.editForm.get(['practicies'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOffers>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: SelectableEntity[], option: SelectableEntity): SelectableEntity {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
