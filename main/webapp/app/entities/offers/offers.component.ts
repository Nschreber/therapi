import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOffers } from 'app/shared/model/offers.model';
import { OffersService } from './offers.service';
import { OffersDeleteDialogComponent } from './offers-delete-dialog.component';

@Component({
  selector: 'jhi-offers',
  templateUrl: './offers.component.html'
})
export class OffersComponent implements OnInit, OnDestroy {
  offers?: IOffers[];
  eventSubscriber?: Subscription;

  constructor(protected offersService: OffersService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.offersService.query().subscribe((res: HttpResponse<IOffers[]>) => (this.offers = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInOffers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOffers): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOffers(): void {
    this.eventSubscriber = this.eventManager.subscribe('offersListModification', () => this.loadAll());
  }

  delete(offers: IOffers): void {
    const modalRef = this.modalService.open(OffersDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.offers = offers;
  }
}
