import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ILeave, Leave } from 'app/shared/model/leave.model';
import { LeaveService } from './leave.service';
import { LeaveComponent } from './leave.component';
import { LeaveDetailComponent } from './leave-detail.component';
import { LeaveUpdateComponent } from './leave-update.component';

@Injectable({ providedIn: 'root' })
export class LeaveResolve implements Resolve<ILeave> {
  constructor(private service: LeaveService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILeave> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((leave: HttpResponse<Leave>) => {
          if (leave.body) {
            return of(leave.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Leave());
  }
}

export const leaveRoute: Routes = [
  {
    path: '',
    component: LeaveComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.leave.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: LeaveDetailComponent,
    resolve: {
      leave: LeaveResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.leave.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: LeaveUpdateComponent,
    resolve: {
      leave: LeaveResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.leave.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: LeaveUpdateComponent,
    resolve: {
      leave: LeaveResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.leave.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
