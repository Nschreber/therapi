import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ILeave } from 'app/shared/model/leave.model';
import { LeaveService } from './leave.service';
import { LeaveDeleteDialogComponent } from './leave-delete-dialog.component';

@Component({
  selector: 'jhi-leave',
  templateUrl: './leave.component.html'
})
export class LeaveComponent implements OnInit, OnDestroy {
  leaves?: ILeave[];
  eventSubscriber?: Subscription;

  constructor(protected leaveService: LeaveService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.leaveService.query().subscribe((res: HttpResponse<ILeave[]>) => (this.leaves = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInLeaves();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ILeave): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInLeaves(): void {
    this.eventSubscriber = this.eventManager.subscribe('leaveListModification', () => this.loadAll());
  }

  delete(leave: ILeave): void {
    const modalRef = this.modalService.open(LeaveDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.leave = leave;
  }
}
