import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ILeave } from 'app/shared/model/leave.model';

type EntityResponseType = HttpResponse<ILeave>;
type EntityArrayResponseType = HttpResponse<ILeave[]>;

@Injectable({ providedIn: 'root' })
export class LeaveService {
  public resourceUrl = SERVER_API_URL + 'api/leaves';

  constructor(protected http: HttpClient) {}

  create(leave: ILeave): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(leave);
    return this.http
      .post<ILeave>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(leave: ILeave): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(leave);
    return this.http
      .put<ILeave>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ILeave>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ILeave[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(leave: ILeave): ILeave {
    const copy: ILeave = Object.assign({}, leave, {
      leaveFrom: leave.leaveFrom && leave.leaveFrom.isValid() ? leave.leaveFrom.format(DATE_FORMAT) : undefined,
      leaveTo: leave.leaveTo && leave.leaveTo.isValid() ? leave.leaveTo.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.leaveFrom = res.body.leaveFrom ? moment(res.body.leaveFrom) : undefined;
      res.body.leaveTo = res.body.leaveTo ? moment(res.body.leaveTo) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((leave: ILeave) => {
        leave.leaveFrom = leave.leaveFrom ? moment(leave.leaveFrom) : undefined;
        leave.leaveTo = leave.leaveTo ? moment(leave.leaveTo) : undefined;
      });
    }
    return res;
  }
}
