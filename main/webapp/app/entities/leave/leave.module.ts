import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TheraPiSharedModule } from 'app/shared/shared.module';
import { LeaveComponent } from './leave.component';
import { LeaveDetailComponent } from './leave-detail.component';
import { LeaveUpdateComponent } from './leave-update.component';
import { LeaveDeleteDialogComponent } from './leave-delete-dialog.component';
import { leaveRoute } from './leave.route';

@NgModule({
  imports: [TheraPiSharedModule, RouterModule.forChild(leaveRoute)],
  declarations: [LeaveComponent, LeaveDetailComponent, LeaveUpdateComponent, LeaveDeleteDialogComponent],
  entryComponents: [LeaveDeleteDialogComponent]
})
export class TheraPiLeaveModule {}
