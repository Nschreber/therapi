import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILeave } from 'app/shared/model/leave.model';
import { LeaveService } from './leave.service';

@Component({
  templateUrl: './leave-delete-dialog.component.html'
})
export class LeaveDeleteDialogComponent {
  leave?: ILeave;

  constructor(protected leaveService: LeaveService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.leaveService.delete(id).subscribe(() => {
      this.eventManager.broadcast('leaveListModification');
      this.activeModal.close();
    });
  }
}
