import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ILeave, Leave } from 'app/shared/model/leave.model';
import { LeaveService } from './leave.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-leave-update',
  templateUrl: './leave-update.component.html'
})
export class LeaveUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  leaveFromDp: any;
  leaveToDp: any;

  editForm = this.fb.group({
    id: [],
    leaveFrom: [],
    leaveTo: [],
    reason: [],
    user: []
  });

  constructor(
    protected leaveService: LeaveService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ leave }) => {
      this.updateForm(leave);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(leave: ILeave): void {
    this.editForm.patchValue({
      id: leave.id,
      leaveFrom: leave.leaveFrom,
      leaveTo: leave.leaveTo,
      reason: leave.reason,
      user: leave.user
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const leave = this.createFromForm();
    if (leave.id !== undefined) {
      this.subscribeToSaveResponse(this.leaveService.update(leave));
    } else {
      this.subscribeToSaveResponse(this.leaveService.create(leave));
    }
  }

  private createFromForm(): ILeave {
    return {
      ...new Leave(),
      id: this.editForm.get(['id'])!.value,
      leaveFrom: this.editForm.get(['leaveFrom'])!.value,
      leaveTo: this.editForm.get(['leaveTo'])!.value,
      reason: this.editForm.get(['reason'])!.value,
      user: this.editForm.get(['user'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILeave>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
