import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAttendence, Attendence } from 'app/shared/model/attendence.model';
import { AttendenceService } from './attendence.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-attendence-update',
  templateUrl: './attendence-update.component.html'
})
export class AttendenceUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    timeFrom: [],
    timeTo: [],
    user: []
  });

  constructor(
    protected attendenceService: AttendenceService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ attendence }) => {
      if (!attendence.id) {
        const today = moment().startOf('day');
        attendence.timeFrom = today;
        attendence.timeTo = today;
      }

      this.updateForm(attendence);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(attendence: IAttendence): void {
    this.editForm.patchValue({
      id: attendence.id,
      timeFrom: attendence.timeFrom ? attendence.timeFrom.format(DATE_TIME_FORMAT) : null,
      timeTo: attendence.timeTo ? attendence.timeTo.format(DATE_TIME_FORMAT) : null,
      user: attendence.user
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const attendence = this.createFromForm();
    if (attendence.id !== undefined) {
      this.subscribeToSaveResponse(this.attendenceService.update(attendence));
    } else {
      this.subscribeToSaveResponse(this.attendenceService.create(attendence));
    }
  }

  private createFromForm(): IAttendence {
    return {
      ...new Attendence(),
      id: this.editForm.get(['id'])!.value,
      timeFrom: this.editForm.get(['timeFrom'])!.value ? moment(this.editForm.get(['timeFrom'])!.value, DATE_TIME_FORMAT) : undefined,
      timeTo: this.editForm.get(['timeTo'])!.value ? moment(this.editForm.get(['timeTo'])!.value, DATE_TIME_FORMAT) : undefined,
      user: this.editForm.get(['user'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAttendence>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
