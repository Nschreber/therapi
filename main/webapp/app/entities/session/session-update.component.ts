import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISession, Session } from 'app/shared/model/session.model';
import { SessionService } from './session.service';
import { IPatient } from 'app/shared/model/patient.model';
import { PatientService } from 'app/entities/patient/patient.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IOffers } from 'app/shared/model/offers.model';
import { OffersService } from 'app/entities/offers/offers.service';

type SelectableEntity = IPatient | IUser | IOffers;

@Component({
  selector: 'jhi-session-update',
  templateUrl: './session-update.component.html'
})
export class SessionUpdateComponent implements OnInit {
  isSaving = false;
  patients: IPatient[] = [];
  users: IUser[] = [];
  offers: IOffers[] = [];

  editForm = this.fb.group({
    id: [],
    timeFrom: [],
    timeTo: [],
    notes: [],
    patient: [],
    user: [],
    offer: []
  });

  constructor(
    protected sessionService: SessionService,
    protected patientService: PatientService,
    protected userService: UserService,
    protected offersService: OffersService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ session }) => {
      if (!session.id) {
        const today = moment().startOf('day');
        session.timeFrom = today;
        session.timeTo = today;
      }

      this.updateForm(session);

      this.patientService.query().subscribe((res: HttpResponse<IPatient[]>) => (this.patients = res.body || []));

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.offersService.query().subscribe((res: HttpResponse<IOffers[]>) => (this.offers = res.body || []));
    });
  }

  updateForm(session: ISession): void {
    this.editForm.patchValue({
      id: session.id,
      timeFrom: session.timeFrom ? session.timeFrom.format(DATE_TIME_FORMAT) : null,
      timeTo: session.timeTo ? session.timeTo.format(DATE_TIME_FORMAT) : null,
      notes: session.notes,
      patient: session.patient,
      user: session.user,
      offer: session.offer
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const session = this.createFromForm();
    if (session.id !== undefined) {
      this.subscribeToSaveResponse(this.sessionService.update(session));
    } else {
      this.subscribeToSaveResponse(this.sessionService.create(session));
    }
  }

  private createFromForm(): ISession {
    return {
      ...new Session(),
      id: this.editForm.get(['id'])!.value,
      timeFrom: this.editForm.get(['timeFrom'])!.value ? moment(this.editForm.get(['timeFrom'])!.value, DATE_TIME_FORMAT) : undefined,
      timeTo: this.editForm.get(['timeTo'])!.value ? moment(this.editForm.get(['timeTo'])!.value, DATE_TIME_FORMAT) : undefined,
      notes: this.editForm.get(['notes'])!.value,
      patient: this.editForm.get(['patient'])!.value,
      user: this.editForm.get(['user'])!.value,
      offer: this.editForm.get(['offer'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISession>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
