import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPracticies, Practicies } from 'app/shared/model/practicies.model';
import { PracticiesService } from './practicies.service';

@Component({
  selector: 'jhi-practicies-update',
  templateUrl: './practicies-update.component.html'
})
export class PracticiesUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    institutionalFlag: []
  });

  constructor(protected practiciesService: PracticiesService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ practicies }) => {
      this.updateForm(practicies);
    });
  }

  updateForm(practicies: IPracticies): void {
    this.editForm.patchValue({
      id: practicies.id,
      name: practicies.name,
      institutionalFlag: practicies.institutionalFlag
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const practicies = this.createFromForm();
    if (practicies.id !== undefined) {
      this.subscribeToSaveResponse(this.practiciesService.update(practicies));
    } else {
      this.subscribeToSaveResponse(this.practiciesService.create(practicies));
    }
  }

  private createFromForm(): IPracticies {
    return {
      ...new Practicies(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      institutionalFlag: this.editForm.get(['institutionalFlag'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPracticies>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
