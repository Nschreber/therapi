import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPracticies } from 'app/shared/model/practicies.model';

@Component({
  selector: 'jhi-practicies-detail',
  templateUrl: './practicies-detail.component.html'
})
export class PracticiesDetailComponent implements OnInit {
  practicies: IPracticies | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ practicies }) => (this.practicies = practicies));
  }

  previousState(): void {
    window.history.back();
  }
}
