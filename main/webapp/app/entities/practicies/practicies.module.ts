import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TheraPiSharedModule } from 'app/shared/shared.module';
import { PracticiesComponent } from './practicies.component';
import { PracticiesDetailComponent } from './practicies-detail.component';
import { PracticiesUpdateComponent } from './practicies-update.component';
import { PracticiesDeleteDialogComponent } from './practicies-delete-dialog.component';
import { practiciesRoute } from './practicies.route';

@NgModule({
  imports: [TheraPiSharedModule, RouterModule.forChild(practiciesRoute)],
  declarations: [PracticiesComponent, PracticiesDetailComponent, PracticiesUpdateComponent, PracticiesDeleteDialogComponent],
  entryComponents: [PracticiesDeleteDialogComponent]
})
export class TheraPiPracticiesModule {}
