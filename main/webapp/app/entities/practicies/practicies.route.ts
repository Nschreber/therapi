import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPracticies, Practicies } from 'app/shared/model/practicies.model';
import { PracticiesService } from './practicies.service';
import { PracticiesComponent } from './practicies.component';
import { PracticiesDetailComponent } from './practicies-detail.component';
import { PracticiesUpdateComponent } from './practicies-update.component';

@Injectable({ providedIn: 'root' })
export class PracticiesResolve implements Resolve<IPracticies> {
  constructor(private service: PracticiesService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPracticies> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((practicies: HttpResponse<Practicies>) => {
          if (practicies.body) {
            return of(practicies.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Practicies());
  }
}

export const practiciesRoute: Routes = [
  {
    path: '',
    component: PracticiesComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.practicies.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PracticiesDetailComponent,
    resolve: {
      practicies: PracticiesResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.practicies.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PracticiesUpdateComponent,
    resolve: {
      practicies: PracticiesResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.practicies.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PracticiesUpdateComponent,
    resolve: {
      practicies: PracticiesResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.practicies.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
