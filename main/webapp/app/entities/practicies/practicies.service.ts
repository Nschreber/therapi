import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPracticies } from 'app/shared/model/practicies.model';

type EntityResponseType = HttpResponse<IPracticies>;
type EntityArrayResponseType = HttpResponse<IPracticies[]>;

@Injectable({ providedIn: 'root' })
export class PracticiesService {
  public resourceUrl = SERVER_API_URL + 'api/practicies';

  constructor(protected http: HttpClient) {}

  create(practicies: IPracticies): Observable<EntityResponseType> {
    return this.http.post<IPracticies>(this.resourceUrl, practicies, { observe: 'response' });
  }

  update(practicies: IPracticies): Observable<EntityResponseType> {
    return this.http.put<IPracticies>(this.resourceUrl, practicies, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPracticies>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPracticies[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
