import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPracticies } from 'app/shared/model/practicies.model';
import { PracticiesService } from './practicies.service';

@Component({
  templateUrl: './practicies-delete-dialog.component.html'
})
export class PracticiesDeleteDialogComponent {
  practicies?: IPracticies;

  constructor(
    protected practiciesService: PracticiesService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.practiciesService.delete(id).subscribe(() => {
      this.eventManager.broadcast('practiciesListModification');
      this.activeModal.close();
    });
  }
}
