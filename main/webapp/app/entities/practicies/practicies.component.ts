import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPracticies } from 'app/shared/model/practicies.model';
import { PracticiesService } from './practicies.service';
import { PracticiesDeleteDialogComponent } from './practicies-delete-dialog.component';

@Component({
  selector: 'jhi-practicies',
  templateUrl: './practicies.component.html'
})
export class PracticiesComponent implements OnInit, OnDestroy {
  practicies?: IPracticies[];
  eventSubscriber?: Subscription;

  constructor(protected practiciesService: PracticiesService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.practiciesService.query().subscribe((res: HttpResponse<IPracticies[]>) => (this.practicies = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPracticies();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPracticies): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPracticies(): void {
    this.eventSubscriber = this.eventManager.subscribe('practiciesListModification', () => this.loadAll());
  }

  delete(practicies: IPracticies): void {
    const modalRef = this.modalService.open(PracticiesDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.practicies = practicies;
  }
}
