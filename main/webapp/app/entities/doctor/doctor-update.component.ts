import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IDoctor, Doctor } from 'app/shared/model/doctor.model';
import { DoctorService } from './doctor.service';

@Component({
  selector: 'jhi-doctor-update',
  templateUrl: './doctor-update.component.html'
})
export class DoctorUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    title: [],
    firstName: [],
    lastName: [],
    phoneNumber: [],
    emailAddress: [],
    addressCountry: [],
    addressState: [],
    addressPostalCode: [],
    addressStreet: [],
    addressHouseNumber: [],
    addressAdditive: []
  });

  constructor(protected doctorService: DoctorService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ doctor }) => {
      this.updateForm(doctor);
    });
  }

  updateForm(doctor: IDoctor): void {
    this.editForm.patchValue({
      id: doctor.id,
      title: doctor.title,
      firstName: doctor.firstName,
      lastName: doctor.lastName,
      phoneNumber: doctor.phoneNumber,
      emailAddress: doctor.emailAddress,
      addressCountry: doctor.addressCountry,
      addressState: doctor.addressState,
      addressPostalCode: doctor.addressPostalCode,
      addressStreet: doctor.addressStreet,
      addressHouseNumber: doctor.addressHouseNumber,
      addressAdditive: doctor.addressAdditive
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const doctor = this.createFromForm();
    if (doctor.id !== undefined) {
      this.subscribeToSaveResponse(this.doctorService.update(doctor));
    } else {
      this.subscribeToSaveResponse(this.doctorService.create(doctor));
    }
  }

  private createFromForm(): IDoctor {
    return {
      ...new Doctor(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      emailAddress: this.editForm.get(['emailAddress'])!.value,
      addressCountry: this.editForm.get(['addressCountry'])!.value,
      addressState: this.editForm.get(['addressState'])!.value,
      addressPostalCode: this.editForm.get(['addressPostalCode'])!.value,
      addressStreet: this.editForm.get(['addressStreet'])!.value,
      addressHouseNumber: this.editForm.get(['addressHouseNumber'])!.value,
      addressAdditive: this.editForm.get(['addressAdditive'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDoctor>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
