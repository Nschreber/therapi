import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IStates } from 'app/shared/model/states.model';
import { StatesService } from './states.service';
import { StatesDeleteDialogComponent } from './states-delete-dialog.component';

@Component({
  selector: 'jhi-states',
  templateUrl: './states.component.html'
})
export class StatesComponent implements OnInit, OnDestroy {
  states?: IStates[];
  eventSubscriber?: Subscription;

  constructor(protected statesService: StatesService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.statesService.query().subscribe((res: HttpResponse<IStates[]>) => (this.states = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInStates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IStates): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInStates(): void {
    this.eventSubscriber = this.eventManager.subscribe('statesListModification', () => this.loadAll());
  }

  delete(states: IStates): void {
    const modalRef = this.modalService.open(StatesDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.states = states;
  }
}
