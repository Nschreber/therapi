import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TheraPiSharedModule } from 'app/shared/shared.module';
import { StatesComponent } from './states.component';
import { StatesDetailComponent } from './states-detail.component';
import { StatesUpdateComponent } from './states-update.component';
import { StatesDeleteDialogComponent } from './states-delete-dialog.component';
import { statesRoute } from './states.route';

@NgModule({
  imports: [TheraPiSharedModule, RouterModule.forChild(statesRoute)],
  declarations: [StatesComponent, StatesDetailComponent, StatesUpdateComponent, StatesDeleteDialogComponent],
  entryComponents: [StatesDeleteDialogComponent]
})
export class TheraPiStatesModule {}
