import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IStates, States } from 'app/shared/model/states.model';
import { StatesService } from './states.service';
import { StatesComponent } from './states.component';
import { StatesDetailComponent } from './states-detail.component';
import { StatesUpdateComponent } from './states-update.component';

@Injectable({ providedIn: 'root' })
export class StatesResolve implements Resolve<IStates> {
  constructor(private service: StatesService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStates> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((states: HttpResponse<States>) => {
          if (states.body) {
            return of(states.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new States());
  }
}

export const statesRoute: Routes = [
  {
    path: '',
    component: StatesComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.states.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: StatesDetailComponent,
    resolve: {
      states: StatesResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.states.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: StatesUpdateComponent,
    resolve: {
      states: StatesResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.states.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: StatesUpdateComponent,
    resolve: {
      states: StatesResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'theraPiApp.states.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
