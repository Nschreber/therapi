import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IStates } from 'app/shared/model/states.model';

type EntityResponseType = HttpResponse<IStates>;
type EntityArrayResponseType = HttpResponse<IStates[]>;

@Injectable({ providedIn: 'root' })
export class StatesService {
  public resourceUrl = SERVER_API_URL + 'api/states';

  constructor(protected http: HttpClient) {}

  create(states: IStates): Observable<EntityResponseType> {
    return this.http.post<IStates>(this.resourceUrl, states, { observe: 'response' });
  }

  update(states: IStates): Observable<EntityResponseType> {
    return this.http.put<IStates>(this.resourceUrl, states, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IStates>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IStates[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
