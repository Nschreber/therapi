import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IStates } from 'app/shared/model/states.model';

@Component({
  selector: 'jhi-states-detail',
  templateUrl: './states-detail.component.html'
})
export class StatesDetailComponent implements OnInit {
  states: IStates | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ states }) => (this.states = states));
  }

  previousState(): void {
    window.history.back();
  }
}
