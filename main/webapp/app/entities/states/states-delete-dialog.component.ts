import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IStates } from 'app/shared/model/states.model';
import { StatesService } from './states.service';

@Component({
  templateUrl: './states-delete-dialog.component.html'
})
export class StatesDeleteDialogComponent {
  states?: IStates;

  constructor(protected statesService: StatesService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.statesService.delete(id).subscribe(() => {
      this.eventManager.broadcast('statesListModification');
      this.activeModal.close();
    });
  }
}
