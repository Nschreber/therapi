import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IStates, States } from 'app/shared/model/states.model';
import { StatesService } from './states.service';

@Component({
  selector: 'jhi-states-update',
  templateUrl: './states-update.component.html'
})
export class StatesUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    stateName: []
  });

  constructor(protected statesService: StatesService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ states }) => {
      this.updateForm(states);
    });
  }

  updateForm(states: IStates): void {
    this.editForm.patchValue({
      id: states.id,
      stateName: states.stateName
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const states = this.createFromForm();
    if (states.id !== undefined) {
      this.subscribeToSaveResponse(this.statesService.update(states));
    } else {
      this.subscribeToSaveResponse(this.statesService.create(states));
    }
  }

  private createFromForm(): IStates {
    return {
      ...new States(),
      id: this.editForm.get(['id'])!.value,
      stateName: this.editForm.get(['stateName'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStates>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
