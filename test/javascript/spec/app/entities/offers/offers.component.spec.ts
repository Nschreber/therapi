import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TheraPiTestModule } from '../../../test.module';
import { OffersComponent } from 'app/entities/offers/offers.component';
import { OffersService } from 'app/entities/offers/offers.service';
import { Offers } from 'app/shared/model/offers.model';

describe('Component Tests', () => {
  describe('Offers Management Component', () => {
    let comp: OffersComponent;
    let fixture: ComponentFixture<OffersComponent>;
    let service: OffersService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [OffersComponent]
      })
        .overrideTemplate(OffersComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OffersComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OffersService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Offers(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.offers && comp.offers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
