import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { OffersService } from 'app/entities/offers/offers.service';
import { IOffers, Offers } from 'app/shared/model/offers.model';

describe('Service Tests', () => {
  describe('Offers Service', () => {
    let injector: TestBed;
    let service: OffersService;
    let httpMock: HttpTestingController;
    let elemDefault: IOffers;
    let expectedResult: IOffers | IOffers[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(OffersService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Offers(0, 'AAAAAAA', 'AAAAAAA', currentDate, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            avalableFrom: currentDate.format(DATE_FORMAT),
            avalableTo: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Offers', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            avalableFrom: currentDate.format(DATE_FORMAT),
            avalableTo: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            avalableFrom: currentDate,
            avalableTo: currentDate
          },
          returnedFromService
        );

        service.create(new Offers()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Offers', () => {
        const returnedFromService = Object.assign(
          {
            offerName: 'BBBBBB',
            offerDescription: 'BBBBBB',
            avalableFrom: currentDate.format(DATE_FORMAT),
            avalableTo: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            avalableFrom: currentDate,
            avalableTo: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Offers', () => {
        const returnedFromService = Object.assign(
          {
            offerName: 'BBBBBB',
            offerDescription: 'BBBBBB',
            avalableFrom: currentDate.format(DATE_FORMAT),
            avalableTo: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            avalableFrom: currentDate,
            avalableTo: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Offers', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
