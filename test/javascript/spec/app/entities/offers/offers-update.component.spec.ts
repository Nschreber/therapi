import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TheraPiTestModule } from '../../../test.module';
import { OffersUpdateComponent } from 'app/entities/offers/offers-update.component';
import { OffersService } from 'app/entities/offers/offers.service';
import { Offers } from 'app/shared/model/offers.model';

describe('Component Tests', () => {
  describe('Offers Management Update Component', () => {
    let comp: OffersUpdateComponent;
    let fixture: ComponentFixture<OffersUpdateComponent>;
    let service: OffersService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [OffersUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OffersUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OffersUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OffersService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Offers(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Offers();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
