import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TheraPiTestModule } from '../../../test.module';
import { OffersDetailComponent } from 'app/entities/offers/offers-detail.component';
import { Offers } from 'app/shared/model/offers.model';

describe('Component Tests', () => {
  describe('Offers Management Detail Component', () => {
    let comp: OffersDetailComponent;
    let fixture: ComponentFixture<OffersDetailComponent>;
    const route = ({ data: of({ offers: new Offers(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [OffersDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OffersDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OffersDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load offers on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.offers).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
