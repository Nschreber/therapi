import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TheraPiTestModule } from '../../../test.module';
import { StatesComponent } from 'app/entities/states/states.component';
import { StatesService } from 'app/entities/states/states.service';
import { States } from 'app/shared/model/states.model';

describe('Component Tests', () => {
  describe('States Management Component', () => {
    let comp: StatesComponent;
    let fixture: ComponentFixture<StatesComponent>;
    let service: StatesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [StatesComponent]
      })
        .overrideTemplate(StatesComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(StatesComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(StatesService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new States(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.states && comp.states[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
