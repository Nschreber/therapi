import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TheraPiTestModule } from '../../../test.module';
import { StatesDetailComponent } from 'app/entities/states/states-detail.component';
import { States } from 'app/shared/model/states.model';

describe('Component Tests', () => {
  describe('States Management Detail Component', () => {
    let comp: StatesDetailComponent;
    let fixture: ComponentFixture<StatesDetailComponent>;
    const route = ({ data: of({ states: new States(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [StatesDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(StatesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(StatesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load states on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.states).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
