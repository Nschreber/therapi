import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TheraPiTestModule } from '../../../test.module';
import { StatesUpdateComponent } from 'app/entities/states/states-update.component';
import { StatesService } from 'app/entities/states/states.service';
import { States } from 'app/shared/model/states.model';

describe('Component Tests', () => {
  describe('States Management Update Component', () => {
    let comp: StatesUpdateComponent;
    let fixture: ComponentFixture<StatesUpdateComponent>;
    let service: StatesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [StatesUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(StatesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(StatesUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(StatesService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new States(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new States();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
