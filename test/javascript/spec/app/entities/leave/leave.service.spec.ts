import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { LeaveService } from 'app/entities/leave/leave.service';
import { ILeave, Leave } from 'app/shared/model/leave.model';

describe('Service Tests', () => {
  describe('Leave Service', () => {
    let injector: TestBed;
    let service: LeaveService;
    let httpMock: HttpTestingController;
    let elemDefault: ILeave;
    let expectedResult: ILeave | ILeave[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(LeaveService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Leave(0, currentDate, currentDate, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            leaveFrom: currentDate.format(DATE_FORMAT),
            leaveTo: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Leave', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            leaveFrom: currentDate.format(DATE_FORMAT),
            leaveTo: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            leaveFrom: currentDate,
            leaveTo: currentDate
          },
          returnedFromService
        );

        service.create(new Leave()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Leave', () => {
        const returnedFromService = Object.assign(
          {
            leaveFrom: currentDate.format(DATE_FORMAT),
            leaveTo: currentDate.format(DATE_FORMAT),
            reason: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            leaveFrom: currentDate,
            leaveTo: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Leave', () => {
        const returnedFromService = Object.assign(
          {
            leaveFrom: currentDate.format(DATE_FORMAT),
            leaveTo: currentDate.format(DATE_FORMAT),
            reason: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            leaveFrom: currentDate,
            leaveTo: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Leave', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
