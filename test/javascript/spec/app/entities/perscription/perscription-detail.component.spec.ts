import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TheraPiTestModule } from '../../../test.module';
import { PerscriptionDetailComponent } from 'app/entities/perscription/perscription-detail.component';
import { Perscription } from 'app/shared/model/perscription.model';

describe('Component Tests', () => {
  describe('Perscription Management Detail Component', () => {
    let comp: PerscriptionDetailComponent;
    let fixture: ComponentFixture<PerscriptionDetailComponent>;
    const route = ({ data: of({ perscription: new Perscription(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [PerscriptionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PerscriptionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PerscriptionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load perscription on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.perscription).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
