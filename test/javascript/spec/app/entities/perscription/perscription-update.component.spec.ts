import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TheraPiTestModule } from '../../../test.module';
import { PerscriptionUpdateComponent } from 'app/entities/perscription/perscription-update.component';
import { PerscriptionService } from 'app/entities/perscription/perscription.service';
import { Perscription } from 'app/shared/model/perscription.model';

describe('Component Tests', () => {
  describe('Perscription Management Update Component', () => {
    let comp: PerscriptionUpdateComponent;
    let fixture: ComponentFixture<PerscriptionUpdateComponent>;
    let service: PerscriptionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [PerscriptionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PerscriptionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PerscriptionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PerscriptionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Perscription(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Perscription();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
