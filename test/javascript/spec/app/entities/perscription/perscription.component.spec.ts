import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TheraPiTestModule } from '../../../test.module';
import { PerscriptionComponent } from 'app/entities/perscription/perscription.component';
import { PerscriptionService } from 'app/entities/perscription/perscription.service';
import { Perscription } from 'app/shared/model/perscription.model';

describe('Component Tests', () => {
  describe('Perscription Management Component', () => {
    let comp: PerscriptionComponent;
    let fixture: ComponentFixture<PerscriptionComponent>;
    let service: PerscriptionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [PerscriptionComponent]
      })
        .overrideTemplate(PerscriptionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PerscriptionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PerscriptionService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Perscription(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.perscriptions && comp.perscriptions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
