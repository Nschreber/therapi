import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { PerscriptionService } from 'app/entities/perscription/perscription.service';
import { IPerscription, Perscription } from 'app/shared/model/perscription.model';

describe('Service Tests', () => {
  describe('Perscription Service', () => {
    let injector: TestBed;
    let service: PerscriptionService;
    let httpMock: HttpTestingController;
    let elemDefault: IPerscription;
    let expectedResult: IPerscription | IPerscription[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(PerscriptionService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Perscription(
        0,
        'AAAAAAA',
        currentDate,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        false
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            recipieDate: currentDate.format(DATE_FORMAT),
            latestPossibleBegin: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Perscription', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            recipieDate: currentDate.format(DATE_FORMAT),
            latestPossibleBegin: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            recipieDate: currentDate,
            latestPossibleBegin: currentDate
          },
          returnedFromService
        );

        service.create(new Perscription()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Perscription', () => {
        const returnedFromService = Object.assign(
          {
            recipieClass: 'BBBBBB',
            recipieDate: currentDate.format(DATE_FORMAT),
            latestPossibleBegin: currentDate.format(DATE_FORMAT),
            typeOfRegulation: 'BBBBBB',
            perscribedRemedies1: 'BBBBBB',
            perscribedRemedies2: 'BBBBBB',
            perscribedRemedies3: 'BBBBBB',
            perscribedRemedies4: 'BBBBBB',
            perscribedRemediesCount1: 'BBBBBB',
            perscribedRemediesCount2: 'BBBBBB',
            perscribedRemediesCount3: 'BBBBBB',
            perscribedRemediesCount4: 'BBBBBB',
            treatmentFrequency: 'BBBBBB',
            duratationTreatment: 1,
            indicationKey: 'BBBBBB',
            barcodeFormat: 'BBBBBB',
            colorcodeTK: 'BBBBBB',
            icd10Code1: 'BBBBBB',
            icd10Code2: 'BBBBBB',
            diagnosisFromDoctor: 'BBBBBB',
            homeVisit: true,
            report: true
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            recipieDate: currentDate,
            latestPossibleBegin: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Perscription', () => {
        const returnedFromService = Object.assign(
          {
            recipieClass: 'BBBBBB',
            recipieDate: currentDate.format(DATE_FORMAT),
            latestPossibleBegin: currentDate.format(DATE_FORMAT),
            typeOfRegulation: 'BBBBBB',
            perscribedRemedies1: 'BBBBBB',
            perscribedRemedies2: 'BBBBBB',
            perscribedRemedies3: 'BBBBBB',
            perscribedRemedies4: 'BBBBBB',
            perscribedRemediesCount1: 'BBBBBB',
            perscribedRemediesCount2: 'BBBBBB',
            perscribedRemediesCount3: 'BBBBBB',
            perscribedRemediesCount4: 'BBBBBB',
            treatmentFrequency: 'BBBBBB',
            duratationTreatment: 1,
            indicationKey: 'BBBBBB',
            barcodeFormat: 'BBBBBB',
            colorcodeTK: 'BBBBBB',
            icd10Code1: 'BBBBBB',
            icd10Code2: 'BBBBBB',
            diagnosisFromDoctor: 'BBBBBB',
            homeVisit: true,
            report: true
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            recipieDate: currentDate,
            latestPossibleBegin: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Perscription', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
