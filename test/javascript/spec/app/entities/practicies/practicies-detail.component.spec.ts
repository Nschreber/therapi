import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TheraPiTestModule } from '../../../test.module';
import { PracticiesDetailComponent } from 'app/entities/practicies/practicies-detail.component';
import { Practicies } from 'app/shared/model/practicies.model';

describe('Component Tests', () => {
  describe('Practicies Management Detail Component', () => {
    let comp: PracticiesDetailComponent;
    let fixture: ComponentFixture<PracticiesDetailComponent>;
    const route = ({ data: of({ practicies: new Practicies(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [PracticiesDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PracticiesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PracticiesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load practicies on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.practicies).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
