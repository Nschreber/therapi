import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TheraPiTestModule } from '../../../test.module';
import { PracticiesComponent } from 'app/entities/practicies/practicies.component';
import { PracticiesService } from 'app/entities/practicies/practicies.service';
import { Practicies } from 'app/shared/model/practicies.model';

describe('Component Tests', () => {
  describe('Practicies Management Component', () => {
    let comp: PracticiesComponent;
    let fixture: ComponentFixture<PracticiesComponent>;
    let service: PracticiesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [PracticiesComponent]
      })
        .overrideTemplate(PracticiesComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PracticiesComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PracticiesService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Practicies(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.practicies && comp.practicies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
