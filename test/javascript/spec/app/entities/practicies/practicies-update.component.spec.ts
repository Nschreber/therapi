import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TheraPiTestModule } from '../../../test.module';
import { PracticiesUpdateComponent } from 'app/entities/practicies/practicies-update.component';
import { PracticiesService } from 'app/entities/practicies/practicies.service';
import { Practicies } from 'app/shared/model/practicies.model';

describe('Component Tests', () => {
  describe('Practicies Management Update Component', () => {
    let comp: PracticiesUpdateComponent;
    let fixture: ComponentFixture<PracticiesUpdateComponent>;
    let service: PracticiesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TheraPiTestModule],
        declarations: [PracticiesUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PracticiesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PracticiesUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PracticiesService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Practicies(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Practicies();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
