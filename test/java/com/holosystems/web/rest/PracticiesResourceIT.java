package com.holosystems.web.rest;

import com.holosystems.TheraPiApp;
import com.holosystems.domain.Practicies;
import com.holosystems.repository.PracticiesRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PracticiesResource} REST controller.
 */
@SpringBootTest(classes = TheraPiApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class PracticiesResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_INSTITUTIONAL_FLAG = 1;
    private static final Integer UPDATED_INSTITUTIONAL_FLAG = 2;

    @Autowired
    private PracticiesRepository practiciesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPracticiesMockMvc;

    private Practicies practicies;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Practicies createEntity(EntityManager em) {
        Practicies practicies = new Practicies()
            .name(DEFAULT_NAME)
            .institutionalFlag(DEFAULT_INSTITUTIONAL_FLAG);
        return practicies;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Practicies createUpdatedEntity(EntityManager em) {
        Practicies practicies = new Practicies()
            .name(UPDATED_NAME)
            .institutionalFlag(UPDATED_INSTITUTIONAL_FLAG);
        return practicies;
    }

    @BeforeEach
    public void initTest() {
        practicies = createEntity(em);
    }

    @Test
    @Transactional
    public void createPracticies() throws Exception {
        int databaseSizeBeforeCreate = practiciesRepository.findAll().size();

        // Create the Practicies
        restPracticiesMockMvc.perform(post("/api/practicies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(practicies)))
            .andExpect(status().isCreated());

        // Validate the Practicies in the database
        List<Practicies> practiciesList = practiciesRepository.findAll();
        assertThat(practiciesList).hasSize(databaseSizeBeforeCreate + 1);
        Practicies testPracticies = practiciesList.get(practiciesList.size() - 1);
        assertThat(testPracticies.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPracticies.getInstitutionalFlag()).isEqualTo(DEFAULT_INSTITUTIONAL_FLAG);
    }

    @Test
    @Transactional
    public void createPracticiesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = practiciesRepository.findAll().size();

        // Create the Practicies with an existing ID
        practicies.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPracticiesMockMvc.perform(post("/api/practicies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(practicies)))
            .andExpect(status().isBadRequest());

        // Validate the Practicies in the database
        List<Practicies> practiciesList = practiciesRepository.findAll();
        assertThat(practiciesList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPracticies() throws Exception {
        // Initialize the database
        practiciesRepository.saveAndFlush(practicies);

        // Get all the practiciesList
        restPracticiesMockMvc.perform(get("/api/practicies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(practicies.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].institutionalFlag").value(hasItem(DEFAULT_INSTITUTIONAL_FLAG)));
    }
    
    @Test
    @Transactional
    public void getPracticies() throws Exception {
        // Initialize the database
        practiciesRepository.saveAndFlush(practicies);

        // Get the practicies
        restPracticiesMockMvc.perform(get("/api/practicies/{id}", practicies.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(practicies.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.institutionalFlag").value(DEFAULT_INSTITUTIONAL_FLAG));
    }

    @Test
    @Transactional
    public void getNonExistingPracticies() throws Exception {
        // Get the practicies
        restPracticiesMockMvc.perform(get("/api/practicies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePracticies() throws Exception {
        // Initialize the database
        practiciesRepository.saveAndFlush(practicies);

        int databaseSizeBeforeUpdate = practiciesRepository.findAll().size();

        // Update the practicies
        Practicies updatedPracticies = practiciesRepository.findById(practicies.getId()).get();
        // Disconnect from session so that the updates on updatedPracticies are not directly saved in db
        em.detach(updatedPracticies);
        updatedPracticies
            .name(UPDATED_NAME)
            .institutionalFlag(UPDATED_INSTITUTIONAL_FLAG);

        restPracticiesMockMvc.perform(put("/api/practicies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPracticies)))
            .andExpect(status().isOk());

        // Validate the Practicies in the database
        List<Practicies> practiciesList = practiciesRepository.findAll();
        assertThat(practiciesList).hasSize(databaseSizeBeforeUpdate);
        Practicies testPracticies = practiciesList.get(practiciesList.size() - 1);
        assertThat(testPracticies.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPracticies.getInstitutionalFlag()).isEqualTo(UPDATED_INSTITUTIONAL_FLAG);
    }

    @Test
    @Transactional
    public void updateNonExistingPracticies() throws Exception {
        int databaseSizeBeforeUpdate = practiciesRepository.findAll().size();

        // Create the Practicies

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPracticiesMockMvc.perform(put("/api/practicies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(practicies)))
            .andExpect(status().isBadRequest());

        // Validate the Practicies in the database
        List<Practicies> practiciesList = practiciesRepository.findAll();
        assertThat(practiciesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePracticies() throws Exception {
        // Initialize the database
        practiciesRepository.saveAndFlush(practicies);

        int databaseSizeBeforeDelete = practiciesRepository.findAll().size();

        // Delete the practicies
        restPracticiesMockMvc.perform(delete("/api/practicies/{id}", practicies.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Practicies> practiciesList = practiciesRepository.findAll();
        assertThat(practiciesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
