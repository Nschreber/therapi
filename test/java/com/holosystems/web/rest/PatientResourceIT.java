package com.holosystems.web.rest;

import com.holosystems.TheraPiApp;
import com.holosystems.domain.Patient;
import com.holosystems.repository.PatientRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PatientResource} REST controller.
 */
@SpringBootTest(classes = TheraPiApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class PatientResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_PHONE_NUMBER = 1;
    private static final Integer UPDATED_PHONE_NUMBER = 2;

    private static final String DEFAULT_EMAIL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_STATE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_STREET = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_STREET = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_HOUSE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_HOUSE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_ADDITIVE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_ADDITIVE = "BBBBBBBBBB";

    private static final String DEFAULT_HEALTH_INSURANCE = "AAAAAAAAAA";
    private static final String UPDATED_HEALTH_INSURANCE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDAY = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPatientMockMvc;

    private Patient patient;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Patient createEntity(EntityManager em) {
        Patient patient = new Patient()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .emailAddress(DEFAULT_EMAIL_ADDRESS)
            .addressCountry(DEFAULT_ADDRESS_COUNTRY)
            .addressState(DEFAULT_ADDRESS_STATE)
            .addressPostalCode(DEFAULT_ADDRESS_POSTAL_CODE)
            .addressStreet(DEFAULT_ADDRESS_STREET)
            .addressHouseNumber(DEFAULT_ADDRESS_HOUSE_NUMBER)
            .addressAdditive(DEFAULT_ADDRESS_ADDITIVE)
            .healthInsurance(DEFAULT_HEALTH_INSURANCE)
            .birthday(DEFAULT_BIRTHDAY);
        return patient;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Patient createUpdatedEntity(EntityManager em) {
        Patient patient = new Patient()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .emailAddress(UPDATED_EMAIL_ADDRESS)
            .addressCountry(UPDATED_ADDRESS_COUNTRY)
            .addressState(UPDATED_ADDRESS_STATE)
            .addressPostalCode(UPDATED_ADDRESS_POSTAL_CODE)
            .addressStreet(UPDATED_ADDRESS_STREET)
            .addressHouseNumber(UPDATED_ADDRESS_HOUSE_NUMBER)
            .addressAdditive(UPDATED_ADDRESS_ADDITIVE)
            .healthInsurance(UPDATED_HEALTH_INSURANCE)
            .birthday(UPDATED_BIRTHDAY);
        return patient;
    }

    @BeforeEach
    public void initTest() {
        patient = createEntity(em);
    }

    @Test
    @Transactional
    public void createPatient() throws Exception {
        int databaseSizeBeforeCreate = patientRepository.findAll().size();

        // Create the Patient
        restPatientMockMvc.perform(post("/api/patients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(patient)))
            .andExpect(status().isCreated());

        // Validate the Patient in the database
        List<Patient> patientList = patientRepository.findAll();
        assertThat(patientList).hasSize(databaseSizeBeforeCreate + 1);
        Patient testPatient = patientList.get(patientList.size() - 1);
        assertThat(testPatient.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testPatient.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testPatient.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testPatient.getEmailAddress()).isEqualTo(DEFAULT_EMAIL_ADDRESS);
        assertThat(testPatient.getAddressCountry()).isEqualTo(DEFAULT_ADDRESS_COUNTRY);
        assertThat(testPatient.getAddressState()).isEqualTo(DEFAULT_ADDRESS_STATE);
        assertThat(testPatient.getAddressPostalCode()).isEqualTo(DEFAULT_ADDRESS_POSTAL_CODE);
        assertThat(testPatient.getAddressStreet()).isEqualTo(DEFAULT_ADDRESS_STREET);
        assertThat(testPatient.getAddressHouseNumber()).isEqualTo(DEFAULT_ADDRESS_HOUSE_NUMBER);
        assertThat(testPatient.getAddressAdditive()).isEqualTo(DEFAULT_ADDRESS_ADDITIVE);
        assertThat(testPatient.getHealthInsurance()).isEqualTo(DEFAULT_HEALTH_INSURANCE);
        assertThat(testPatient.getBirthday()).isEqualTo(DEFAULT_BIRTHDAY);
    }

    @Test
    @Transactional
    public void createPatientWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = patientRepository.findAll().size();

        // Create the Patient with an existing ID
        patient.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPatientMockMvc.perform(post("/api/patients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(patient)))
            .andExpect(status().isBadRequest());

        // Validate the Patient in the database
        List<Patient> patientList = patientRepository.findAll();
        assertThat(patientList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPatients() throws Exception {
        // Initialize the database
        patientRepository.saveAndFlush(patient);

        // Get all the patientList
        restPatientMockMvc.perform(get("/api/patients?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(patient.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].emailAddress").value(hasItem(DEFAULT_EMAIL_ADDRESS)))
            .andExpect(jsonPath("$.[*].addressCountry").value(hasItem(DEFAULT_ADDRESS_COUNTRY)))
            .andExpect(jsonPath("$.[*].addressState").value(hasItem(DEFAULT_ADDRESS_STATE)))
            .andExpect(jsonPath("$.[*].addressPostalCode").value(hasItem(DEFAULT_ADDRESS_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].addressStreet").value(hasItem(DEFAULT_ADDRESS_STREET)))
            .andExpect(jsonPath("$.[*].addressHouseNumber").value(hasItem(DEFAULT_ADDRESS_HOUSE_NUMBER)))
            .andExpect(jsonPath("$.[*].addressAdditive").value(hasItem(DEFAULT_ADDRESS_ADDITIVE)))
            .andExpect(jsonPath("$.[*].healthInsurance").value(hasItem(DEFAULT_HEALTH_INSURANCE)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())));
    }
    
    @Test
    @Transactional
    public void getPatient() throws Exception {
        // Initialize the database
        patientRepository.saveAndFlush(patient);

        // Get the patient
        restPatientMockMvc.perform(get("/api/patients/{id}", patient.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(patient.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.emailAddress").value(DEFAULT_EMAIL_ADDRESS))
            .andExpect(jsonPath("$.addressCountry").value(DEFAULT_ADDRESS_COUNTRY))
            .andExpect(jsonPath("$.addressState").value(DEFAULT_ADDRESS_STATE))
            .andExpect(jsonPath("$.addressPostalCode").value(DEFAULT_ADDRESS_POSTAL_CODE))
            .andExpect(jsonPath("$.addressStreet").value(DEFAULT_ADDRESS_STREET))
            .andExpect(jsonPath("$.addressHouseNumber").value(DEFAULT_ADDRESS_HOUSE_NUMBER))
            .andExpect(jsonPath("$.addressAdditive").value(DEFAULT_ADDRESS_ADDITIVE))
            .andExpect(jsonPath("$.healthInsurance").value(DEFAULT_HEALTH_INSURANCE))
            .andExpect(jsonPath("$.birthday").value(DEFAULT_BIRTHDAY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPatient() throws Exception {
        // Get the patient
        restPatientMockMvc.perform(get("/api/patients/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePatient() throws Exception {
        // Initialize the database
        patientRepository.saveAndFlush(patient);

        int databaseSizeBeforeUpdate = patientRepository.findAll().size();

        // Update the patient
        Patient updatedPatient = patientRepository.findById(patient.getId()).get();
        // Disconnect from session so that the updates on updatedPatient are not directly saved in db
        em.detach(updatedPatient);
        updatedPatient
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .emailAddress(UPDATED_EMAIL_ADDRESS)
            .addressCountry(UPDATED_ADDRESS_COUNTRY)
            .addressState(UPDATED_ADDRESS_STATE)
            .addressPostalCode(UPDATED_ADDRESS_POSTAL_CODE)
            .addressStreet(UPDATED_ADDRESS_STREET)
            .addressHouseNumber(UPDATED_ADDRESS_HOUSE_NUMBER)
            .addressAdditive(UPDATED_ADDRESS_ADDITIVE)
            .healthInsurance(UPDATED_HEALTH_INSURANCE)
            .birthday(UPDATED_BIRTHDAY);

        restPatientMockMvc.perform(put("/api/patients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPatient)))
            .andExpect(status().isOk());

        // Validate the Patient in the database
        List<Patient> patientList = patientRepository.findAll();
        assertThat(patientList).hasSize(databaseSizeBeforeUpdate);
        Patient testPatient = patientList.get(patientList.size() - 1);
        assertThat(testPatient.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testPatient.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testPatient.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testPatient.getEmailAddress()).isEqualTo(UPDATED_EMAIL_ADDRESS);
        assertThat(testPatient.getAddressCountry()).isEqualTo(UPDATED_ADDRESS_COUNTRY);
        assertThat(testPatient.getAddressState()).isEqualTo(UPDATED_ADDRESS_STATE);
        assertThat(testPatient.getAddressPostalCode()).isEqualTo(UPDATED_ADDRESS_POSTAL_CODE);
        assertThat(testPatient.getAddressStreet()).isEqualTo(UPDATED_ADDRESS_STREET);
        assertThat(testPatient.getAddressHouseNumber()).isEqualTo(UPDATED_ADDRESS_HOUSE_NUMBER);
        assertThat(testPatient.getAddressAdditive()).isEqualTo(UPDATED_ADDRESS_ADDITIVE);
        assertThat(testPatient.getHealthInsurance()).isEqualTo(UPDATED_HEALTH_INSURANCE);
        assertThat(testPatient.getBirthday()).isEqualTo(UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void updateNonExistingPatient() throws Exception {
        int databaseSizeBeforeUpdate = patientRepository.findAll().size();

        // Create the Patient

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPatientMockMvc.perform(put("/api/patients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(patient)))
            .andExpect(status().isBadRequest());

        // Validate the Patient in the database
        List<Patient> patientList = patientRepository.findAll();
        assertThat(patientList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePatient() throws Exception {
        // Initialize the database
        patientRepository.saveAndFlush(patient);

        int databaseSizeBeforeDelete = patientRepository.findAll().size();

        // Delete the patient
        restPatientMockMvc.perform(delete("/api/patients/{id}", patient.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Patient> patientList = patientRepository.findAll();
        assertThat(patientList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
