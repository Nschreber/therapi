package com.holosystems.web.rest;

import com.holosystems.TheraPiApp;
import com.holosystems.domain.Offers;
import com.holosystems.repository.OffersRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OffersResource} REST controller.
 */
@SpringBootTest(classes = TheraPiApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class OffersResourceIT {

    private static final String DEFAULT_OFFER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_OFFER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_OFFER_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_OFFER_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_AVALABLE_FROM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_AVALABLE_FROM = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_AVALABLE_TO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_AVALABLE_TO = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private OffersRepository offersRepository;

    @Mock
    private OffersRepository offersRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOffersMockMvc;

    private Offers offers;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Offers createEntity(EntityManager em) {
        Offers offers = new Offers()
            .offerName(DEFAULT_OFFER_NAME)
            .offerDescription(DEFAULT_OFFER_DESCRIPTION)
            .avalableFrom(DEFAULT_AVALABLE_FROM)
            .avalableTo(DEFAULT_AVALABLE_TO);
        return offers;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Offers createUpdatedEntity(EntityManager em) {
        Offers offers = new Offers()
            .offerName(UPDATED_OFFER_NAME)
            .offerDescription(UPDATED_OFFER_DESCRIPTION)
            .avalableFrom(UPDATED_AVALABLE_FROM)
            .avalableTo(UPDATED_AVALABLE_TO);
        return offers;
    }

    @BeforeEach
    public void initTest() {
        offers = createEntity(em);
    }

    @Test
    @Transactional
    public void createOffers() throws Exception {
        int databaseSizeBeforeCreate = offersRepository.findAll().size();

        // Create the Offers
        restOffersMockMvc.perform(post("/api/offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(offers)))
            .andExpect(status().isCreated());

        // Validate the Offers in the database
        List<Offers> offersList = offersRepository.findAll();
        assertThat(offersList).hasSize(databaseSizeBeforeCreate + 1);
        Offers testOffers = offersList.get(offersList.size() - 1);
        assertThat(testOffers.getOfferName()).isEqualTo(DEFAULT_OFFER_NAME);
        assertThat(testOffers.getOfferDescription()).isEqualTo(DEFAULT_OFFER_DESCRIPTION);
        assertThat(testOffers.getAvalableFrom()).isEqualTo(DEFAULT_AVALABLE_FROM);
        assertThat(testOffers.getAvalableTo()).isEqualTo(DEFAULT_AVALABLE_TO);
    }

    @Test
    @Transactional
    public void createOffersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = offersRepository.findAll().size();

        // Create the Offers with an existing ID
        offers.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOffersMockMvc.perform(post("/api/offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(offers)))
            .andExpect(status().isBadRequest());

        // Validate the Offers in the database
        List<Offers> offersList = offersRepository.findAll();
        assertThat(offersList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOffers() throws Exception {
        // Initialize the database
        offersRepository.saveAndFlush(offers);

        // Get all the offersList
        restOffersMockMvc.perform(get("/api/offers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(offers.getId().intValue())))
            .andExpect(jsonPath("$.[*].offerName").value(hasItem(DEFAULT_OFFER_NAME)))
            .andExpect(jsonPath("$.[*].offerDescription").value(hasItem(DEFAULT_OFFER_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].avalableFrom").value(hasItem(DEFAULT_AVALABLE_FROM.toString())))
            .andExpect(jsonPath("$.[*].avalableTo").value(hasItem(DEFAULT_AVALABLE_TO.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllOffersWithEagerRelationshipsIsEnabled() throws Exception {
        OffersResource offersResource = new OffersResource(offersRepositoryMock);
        when(offersRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restOffersMockMvc.perform(get("/api/offers?eagerload=true"))
            .andExpect(status().isOk());

        verify(offersRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllOffersWithEagerRelationshipsIsNotEnabled() throws Exception {
        OffersResource offersResource = new OffersResource(offersRepositoryMock);
        when(offersRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restOffersMockMvc.perform(get("/api/offers?eagerload=true"))
            .andExpect(status().isOk());

        verify(offersRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getOffers() throws Exception {
        // Initialize the database
        offersRepository.saveAndFlush(offers);

        // Get the offers
        restOffersMockMvc.perform(get("/api/offers/{id}", offers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(offers.getId().intValue()))
            .andExpect(jsonPath("$.offerName").value(DEFAULT_OFFER_NAME))
            .andExpect(jsonPath("$.offerDescription").value(DEFAULT_OFFER_DESCRIPTION))
            .andExpect(jsonPath("$.avalableFrom").value(DEFAULT_AVALABLE_FROM.toString()))
            .andExpect(jsonPath("$.avalableTo").value(DEFAULT_AVALABLE_TO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOffers() throws Exception {
        // Get the offers
        restOffersMockMvc.perform(get("/api/offers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOffers() throws Exception {
        // Initialize the database
        offersRepository.saveAndFlush(offers);

        int databaseSizeBeforeUpdate = offersRepository.findAll().size();

        // Update the offers
        Offers updatedOffers = offersRepository.findById(offers.getId()).get();
        // Disconnect from session so that the updates on updatedOffers are not directly saved in db
        em.detach(updatedOffers);
        updatedOffers
            .offerName(UPDATED_OFFER_NAME)
            .offerDescription(UPDATED_OFFER_DESCRIPTION)
            .avalableFrom(UPDATED_AVALABLE_FROM)
            .avalableTo(UPDATED_AVALABLE_TO);

        restOffersMockMvc.perform(put("/api/offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOffers)))
            .andExpect(status().isOk());

        // Validate the Offers in the database
        List<Offers> offersList = offersRepository.findAll();
        assertThat(offersList).hasSize(databaseSizeBeforeUpdate);
        Offers testOffers = offersList.get(offersList.size() - 1);
        assertThat(testOffers.getOfferName()).isEqualTo(UPDATED_OFFER_NAME);
        assertThat(testOffers.getOfferDescription()).isEqualTo(UPDATED_OFFER_DESCRIPTION);
        assertThat(testOffers.getAvalableFrom()).isEqualTo(UPDATED_AVALABLE_FROM);
        assertThat(testOffers.getAvalableTo()).isEqualTo(UPDATED_AVALABLE_TO);
    }

    @Test
    @Transactional
    public void updateNonExistingOffers() throws Exception {
        int databaseSizeBeforeUpdate = offersRepository.findAll().size();

        // Create the Offers

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOffersMockMvc.perform(put("/api/offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(offers)))
            .andExpect(status().isBadRequest());

        // Validate the Offers in the database
        List<Offers> offersList = offersRepository.findAll();
        assertThat(offersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOffers() throws Exception {
        // Initialize the database
        offersRepository.saveAndFlush(offers);

        int databaseSizeBeforeDelete = offersRepository.findAll().size();

        // Delete the offers
        restOffersMockMvc.perform(delete("/api/offers/{id}", offers.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Offers> offersList = offersRepository.findAll();
        assertThat(offersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
