package com.holosystems.web.rest;

import com.holosystems.TheraPiApp;
import com.holosystems.domain.Leave;
import com.holosystems.repository.LeaveRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LeaveResource} REST controller.
 */
@SpringBootTest(classes = TheraPiApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class LeaveResourceIT {

    private static final LocalDate DEFAULT_LEAVE_FROM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LEAVE_FROM = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LEAVE_TO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LEAVE_TO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    @Autowired
    private LeaveRepository leaveRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLeaveMockMvc;

    private Leave leave;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Leave createEntity(EntityManager em) {
        Leave leave = new Leave()
            .leaveFrom(DEFAULT_LEAVE_FROM)
            .leaveTo(DEFAULT_LEAVE_TO)
            .reason(DEFAULT_REASON);
        return leave;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Leave createUpdatedEntity(EntityManager em) {
        Leave leave = new Leave()
            .leaveFrom(UPDATED_LEAVE_FROM)
            .leaveTo(UPDATED_LEAVE_TO)
            .reason(UPDATED_REASON);
        return leave;
    }

    @BeforeEach
    public void initTest() {
        leave = createEntity(em);
    }

    @Test
    @Transactional
    public void createLeave() throws Exception {
        int databaseSizeBeforeCreate = leaveRepository.findAll().size();

        // Create the Leave
        restLeaveMockMvc.perform(post("/api/leaves")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(leave)))
            .andExpect(status().isCreated());

        // Validate the Leave in the database
        List<Leave> leaveList = leaveRepository.findAll();
        assertThat(leaveList).hasSize(databaseSizeBeforeCreate + 1);
        Leave testLeave = leaveList.get(leaveList.size() - 1);
        assertThat(testLeave.getLeaveFrom()).isEqualTo(DEFAULT_LEAVE_FROM);
        assertThat(testLeave.getLeaveTo()).isEqualTo(DEFAULT_LEAVE_TO);
        assertThat(testLeave.getReason()).isEqualTo(DEFAULT_REASON);
    }

    @Test
    @Transactional
    public void createLeaveWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = leaveRepository.findAll().size();

        // Create the Leave with an existing ID
        leave.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLeaveMockMvc.perform(post("/api/leaves")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(leave)))
            .andExpect(status().isBadRequest());

        // Validate the Leave in the database
        List<Leave> leaveList = leaveRepository.findAll();
        assertThat(leaveList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllLeaves() throws Exception {
        // Initialize the database
        leaveRepository.saveAndFlush(leave);

        // Get all the leaveList
        restLeaveMockMvc.perform(get("/api/leaves?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(leave.getId().intValue())))
            .andExpect(jsonPath("$.[*].leaveFrom").value(hasItem(DEFAULT_LEAVE_FROM.toString())))
            .andExpect(jsonPath("$.[*].leaveTo").value(hasItem(DEFAULT_LEAVE_TO.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)));
    }
    
    @Test
    @Transactional
    public void getLeave() throws Exception {
        // Initialize the database
        leaveRepository.saveAndFlush(leave);

        // Get the leave
        restLeaveMockMvc.perform(get("/api/leaves/{id}", leave.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(leave.getId().intValue()))
            .andExpect(jsonPath("$.leaveFrom").value(DEFAULT_LEAVE_FROM.toString()))
            .andExpect(jsonPath("$.leaveTo").value(DEFAULT_LEAVE_TO.toString()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON));
    }

    @Test
    @Transactional
    public void getNonExistingLeave() throws Exception {
        // Get the leave
        restLeaveMockMvc.perform(get("/api/leaves/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLeave() throws Exception {
        // Initialize the database
        leaveRepository.saveAndFlush(leave);

        int databaseSizeBeforeUpdate = leaveRepository.findAll().size();

        // Update the leave
        Leave updatedLeave = leaveRepository.findById(leave.getId()).get();
        // Disconnect from session so that the updates on updatedLeave are not directly saved in db
        em.detach(updatedLeave);
        updatedLeave
            .leaveFrom(UPDATED_LEAVE_FROM)
            .leaveTo(UPDATED_LEAVE_TO)
            .reason(UPDATED_REASON);

        restLeaveMockMvc.perform(put("/api/leaves")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLeave)))
            .andExpect(status().isOk());

        // Validate the Leave in the database
        List<Leave> leaveList = leaveRepository.findAll();
        assertThat(leaveList).hasSize(databaseSizeBeforeUpdate);
        Leave testLeave = leaveList.get(leaveList.size() - 1);
        assertThat(testLeave.getLeaveFrom()).isEqualTo(UPDATED_LEAVE_FROM);
        assertThat(testLeave.getLeaveTo()).isEqualTo(UPDATED_LEAVE_TO);
        assertThat(testLeave.getReason()).isEqualTo(UPDATED_REASON);
    }

    @Test
    @Transactional
    public void updateNonExistingLeave() throws Exception {
        int databaseSizeBeforeUpdate = leaveRepository.findAll().size();

        // Create the Leave

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLeaveMockMvc.perform(put("/api/leaves")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(leave)))
            .andExpect(status().isBadRequest());

        // Validate the Leave in the database
        List<Leave> leaveList = leaveRepository.findAll();
        assertThat(leaveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLeave() throws Exception {
        // Initialize the database
        leaveRepository.saveAndFlush(leave);

        int databaseSizeBeforeDelete = leaveRepository.findAll().size();

        // Delete the leave
        restLeaveMockMvc.perform(delete("/api/leaves/{id}", leave.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Leave> leaveList = leaveRepository.findAll();
        assertThat(leaveList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
