package com.holosystems.web.rest;

import com.holosystems.TheraPiApp;
import com.holosystems.domain.States;
import com.holosystems.repository.StatesRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StatesResource} REST controller.
 */
@SpringBootTest(classes = TheraPiApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class StatesResourceIT {

    private static final String DEFAULT_STATE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STATE_NAME = "BBBBBBBBBB";

    @Autowired
    private StatesRepository statesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStatesMockMvc;

    private States states;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static States createEntity(EntityManager em) {
        States states = new States()
            .stateName(DEFAULT_STATE_NAME);
        return states;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static States createUpdatedEntity(EntityManager em) {
        States states = new States()
            .stateName(UPDATED_STATE_NAME);
        return states;
    }

    @BeforeEach
    public void initTest() {
        states = createEntity(em);
    }

    @Test
    @Transactional
    public void createStates() throws Exception {
        int databaseSizeBeforeCreate = statesRepository.findAll().size();

        // Create the States
        restStatesMockMvc.perform(post("/api/states")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(states)))
            .andExpect(status().isCreated());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeCreate + 1);
        States testStates = statesList.get(statesList.size() - 1);
        assertThat(testStates.getStateName()).isEqualTo(DEFAULT_STATE_NAME);
    }

    @Test
    @Transactional
    public void createStatesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = statesRepository.findAll().size();

        // Create the States with an existing ID
        states.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStatesMockMvc.perform(post("/api/states")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(states)))
            .andExpect(status().isBadRequest());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllStates() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get all the statesList
        restStatesMockMvc.perform(get("/api/states?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(states.getId().intValue())))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)));
    }
    
    @Test
    @Transactional
    public void getStates() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        // Get the states
        restStatesMockMvc.perform(get("/api/states/{id}", states.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(states.getId().intValue()))
            .andExpect(jsonPath("$.stateName").value(DEFAULT_STATE_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingStates() throws Exception {
        // Get the states
        restStatesMockMvc.perform(get("/api/states/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStates() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        int databaseSizeBeforeUpdate = statesRepository.findAll().size();

        // Update the states
        States updatedStates = statesRepository.findById(states.getId()).get();
        // Disconnect from session so that the updates on updatedStates are not directly saved in db
        em.detach(updatedStates);
        updatedStates
            .stateName(UPDATED_STATE_NAME);

        restStatesMockMvc.perform(put("/api/states")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedStates)))
            .andExpect(status().isOk());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
        States testStates = statesList.get(statesList.size() - 1);
        assertThat(testStates.getStateName()).isEqualTo(UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingStates() throws Exception {
        int databaseSizeBeforeUpdate = statesRepository.findAll().size();

        // Create the States

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStatesMockMvc.perform(put("/api/states")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(states)))
            .andExpect(status().isBadRequest());

        // Validate the States in the database
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteStates() throws Exception {
        // Initialize the database
        statesRepository.saveAndFlush(states);

        int databaseSizeBeforeDelete = statesRepository.findAll().size();

        // Delete the states
        restStatesMockMvc.perform(delete("/api/states/{id}", states.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<States> statesList = statesRepository.findAll();
        assertThat(statesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
