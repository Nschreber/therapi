package com.holosystems.web.rest;

import com.holosystems.TheraPiApp;
import com.holosystems.domain.Perscription;
import com.holosystems.repository.PerscriptionRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PerscriptionResource} REST controller.
 */
@SpringBootTest(classes = TheraPiApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class PerscriptionResourceIT {

    private static final String DEFAULT_RECIPIE_CLASS = "AAAAAAAAAA";
    private static final String UPDATED_RECIPIE_CLASS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_RECIPIE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_RECIPIE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LATEST_POSSIBLE_BEGIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LATEST_POSSIBLE_BEGIN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TYPE_OF_REGULATION = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_OF_REGULATION = "BBBBBBBBBB";

    private static final String DEFAULT_PERSCRIBED_REMEDIES_1 = "AAAAAAAAAA";
    private static final String UPDATED_PERSCRIBED_REMEDIES_1 = "BBBBBBBBBB";

    private static final String DEFAULT_PERSCRIBED_REMEDIES_2 = "AAAAAAAAAA";
    private static final String UPDATED_PERSCRIBED_REMEDIES_2 = "BBBBBBBBBB";

    private static final String DEFAULT_PERSCRIBED_REMEDIES_3 = "AAAAAAAAAA";
    private static final String UPDATED_PERSCRIBED_REMEDIES_3 = "BBBBBBBBBB";

    private static final String DEFAULT_PERSCRIBED_REMEDIES_4 = "AAAAAAAAAA";
    private static final String UPDATED_PERSCRIBED_REMEDIES_4 = "BBBBBBBBBB";

    private static final String DEFAULT_PERSCRIBED_REMEDIES_COUNT_1 = "AAAAAAAAAA";
    private static final String UPDATED_PERSCRIBED_REMEDIES_COUNT_1 = "BBBBBBBBBB";

    private static final String DEFAULT_PERSCRIBED_REMEDIES_COUNT_2 = "AAAAAAAAAA";
    private static final String UPDATED_PERSCRIBED_REMEDIES_COUNT_2 = "BBBBBBBBBB";

    private static final String DEFAULT_PERSCRIBED_REMEDIES_COUNT_3 = "AAAAAAAAAA";
    private static final String UPDATED_PERSCRIBED_REMEDIES_COUNT_3 = "BBBBBBBBBB";

    private static final String DEFAULT_PERSCRIBED_REMEDIES_COUNT_4 = "AAAAAAAAAA";
    private static final String UPDATED_PERSCRIBED_REMEDIES_COUNT_4 = "BBBBBBBBBB";

    private static final String DEFAULT_TREATMENT_FREQUENCY = "AAAAAAAAAA";
    private static final String UPDATED_TREATMENT_FREQUENCY = "BBBBBBBBBB";

    private static final Integer DEFAULT_DURATATION_TREATMENT = 1;
    private static final Integer UPDATED_DURATATION_TREATMENT = 2;

    private static final String DEFAULT_INDICATION_KEY = "AAAAAAAAAA";
    private static final String UPDATED_INDICATION_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_BARCODE_FORMAT = "AAAAAAAAAA";
    private static final String UPDATED_BARCODE_FORMAT = "BBBBBBBBBB";

    private static final String DEFAULT_COLORCODE_TK = "AAAAAAAAAA";
    private static final String UPDATED_COLORCODE_TK = "BBBBBBBBBB";

    private static final String DEFAULT_ICD_10_CODE_1 = "AAAAAAAAAA";
    private static final String UPDATED_ICD_10_CODE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ICD_10_CODE_2 = "AAAAAAAAAA";
    private static final String UPDATED_ICD_10_CODE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_DIAGNOSIS_FROM_DOCTOR = "AAAAAAAAAA";
    private static final String UPDATED_DIAGNOSIS_FROM_DOCTOR = "BBBBBBBBBB";

    private static final Boolean DEFAULT_HOME_VISIT = false;
    private static final Boolean UPDATED_HOME_VISIT = true;

    private static final Boolean DEFAULT_REPORT = false;
    private static final Boolean UPDATED_REPORT = true;

    @Autowired
    private PerscriptionRepository perscriptionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPerscriptionMockMvc;

    private Perscription perscription;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Perscription createEntity(EntityManager em) {
        Perscription perscription = new Perscription()
            .recipieClass(DEFAULT_RECIPIE_CLASS)
            .recipieDate(DEFAULT_RECIPIE_DATE)
            .latestPossibleBegin(DEFAULT_LATEST_POSSIBLE_BEGIN)
            .typeOfRegulation(DEFAULT_TYPE_OF_REGULATION)
            .perscribedRemedies1(DEFAULT_PERSCRIBED_REMEDIES_1)
            .perscribedRemedies2(DEFAULT_PERSCRIBED_REMEDIES_2)
            .perscribedRemedies3(DEFAULT_PERSCRIBED_REMEDIES_3)
            .perscribedRemedies4(DEFAULT_PERSCRIBED_REMEDIES_4)
            .perscribedRemediesCount1(DEFAULT_PERSCRIBED_REMEDIES_COUNT_1)
            .perscribedRemediesCount2(DEFAULT_PERSCRIBED_REMEDIES_COUNT_2)
            .perscribedRemediesCount3(DEFAULT_PERSCRIBED_REMEDIES_COUNT_3)
            .perscribedRemediesCount4(DEFAULT_PERSCRIBED_REMEDIES_COUNT_4)
            .treatmentFrequency(DEFAULT_TREATMENT_FREQUENCY)
            .duratationTreatment(DEFAULT_DURATATION_TREATMENT)
            .indicationKey(DEFAULT_INDICATION_KEY)
            .barcodeFormat(DEFAULT_BARCODE_FORMAT)
            .colorcodeTK(DEFAULT_COLORCODE_TK)
            .icd10Code1(DEFAULT_ICD_10_CODE_1)
            .icd10Code2(DEFAULT_ICD_10_CODE_2)
            .diagnosisFromDoctor(DEFAULT_DIAGNOSIS_FROM_DOCTOR)
            .homeVisit(DEFAULT_HOME_VISIT)
            .report(DEFAULT_REPORT);
        return perscription;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Perscription createUpdatedEntity(EntityManager em) {
        Perscription perscription = new Perscription()
            .recipieClass(UPDATED_RECIPIE_CLASS)
            .recipieDate(UPDATED_RECIPIE_DATE)
            .latestPossibleBegin(UPDATED_LATEST_POSSIBLE_BEGIN)
            .typeOfRegulation(UPDATED_TYPE_OF_REGULATION)
            .perscribedRemedies1(UPDATED_PERSCRIBED_REMEDIES_1)
            .perscribedRemedies2(UPDATED_PERSCRIBED_REMEDIES_2)
            .perscribedRemedies3(UPDATED_PERSCRIBED_REMEDIES_3)
            .perscribedRemedies4(UPDATED_PERSCRIBED_REMEDIES_4)
            .perscribedRemediesCount1(UPDATED_PERSCRIBED_REMEDIES_COUNT_1)
            .perscribedRemediesCount2(UPDATED_PERSCRIBED_REMEDIES_COUNT_2)
            .perscribedRemediesCount3(UPDATED_PERSCRIBED_REMEDIES_COUNT_3)
            .perscribedRemediesCount4(UPDATED_PERSCRIBED_REMEDIES_COUNT_4)
            .treatmentFrequency(UPDATED_TREATMENT_FREQUENCY)
            .duratationTreatment(UPDATED_DURATATION_TREATMENT)
            .indicationKey(UPDATED_INDICATION_KEY)
            .barcodeFormat(UPDATED_BARCODE_FORMAT)
            .colorcodeTK(UPDATED_COLORCODE_TK)
            .icd10Code1(UPDATED_ICD_10_CODE_1)
            .icd10Code2(UPDATED_ICD_10_CODE_2)
            .diagnosisFromDoctor(UPDATED_DIAGNOSIS_FROM_DOCTOR)
            .homeVisit(UPDATED_HOME_VISIT)
            .report(UPDATED_REPORT);
        return perscription;
    }

    @BeforeEach
    public void initTest() {
        perscription = createEntity(em);
    }

    @Test
    @Transactional
    public void createPerscription() throws Exception {
        int databaseSizeBeforeCreate = perscriptionRepository.findAll().size();

        // Create the Perscription
        restPerscriptionMockMvc.perform(post("/api/perscriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(perscription)))
            .andExpect(status().isCreated());

        // Validate the Perscription in the database
        List<Perscription> perscriptionList = perscriptionRepository.findAll();
        assertThat(perscriptionList).hasSize(databaseSizeBeforeCreate + 1);
        Perscription testPerscription = perscriptionList.get(perscriptionList.size() - 1);
        assertThat(testPerscription.getRecipieClass()).isEqualTo(DEFAULT_RECIPIE_CLASS);
        assertThat(testPerscription.getRecipieDate()).isEqualTo(DEFAULT_RECIPIE_DATE);
        assertThat(testPerscription.getLatestPossibleBegin()).isEqualTo(DEFAULT_LATEST_POSSIBLE_BEGIN);
        assertThat(testPerscription.getTypeOfRegulation()).isEqualTo(DEFAULT_TYPE_OF_REGULATION);
        assertThat(testPerscription.getPerscribedRemedies1()).isEqualTo(DEFAULT_PERSCRIBED_REMEDIES_1);
        assertThat(testPerscription.getPerscribedRemedies2()).isEqualTo(DEFAULT_PERSCRIBED_REMEDIES_2);
        assertThat(testPerscription.getPerscribedRemedies3()).isEqualTo(DEFAULT_PERSCRIBED_REMEDIES_3);
        assertThat(testPerscription.getPerscribedRemedies4()).isEqualTo(DEFAULT_PERSCRIBED_REMEDIES_4);
        assertThat(testPerscription.getPerscribedRemediesCount1()).isEqualTo(DEFAULT_PERSCRIBED_REMEDIES_COUNT_1);
        assertThat(testPerscription.getPerscribedRemediesCount2()).isEqualTo(DEFAULT_PERSCRIBED_REMEDIES_COUNT_2);
        assertThat(testPerscription.getPerscribedRemediesCount3()).isEqualTo(DEFAULT_PERSCRIBED_REMEDIES_COUNT_3);
        assertThat(testPerscription.getPerscribedRemediesCount4()).isEqualTo(DEFAULT_PERSCRIBED_REMEDIES_COUNT_4);
        assertThat(testPerscription.getTreatmentFrequency()).isEqualTo(DEFAULT_TREATMENT_FREQUENCY);
        assertThat(testPerscription.getDuratationTreatment()).isEqualTo(DEFAULT_DURATATION_TREATMENT);
        assertThat(testPerscription.getIndicationKey()).isEqualTo(DEFAULT_INDICATION_KEY);
        assertThat(testPerscription.getBarcodeFormat()).isEqualTo(DEFAULT_BARCODE_FORMAT);
        assertThat(testPerscription.getColorcodeTK()).isEqualTo(DEFAULT_COLORCODE_TK);
        assertThat(testPerscription.getIcd10Code1()).isEqualTo(DEFAULT_ICD_10_CODE_1);
        assertThat(testPerscription.getIcd10Code2()).isEqualTo(DEFAULT_ICD_10_CODE_2);
        assertThat(testPerscription.getDiagnosisFromDoctor()).isEqualTo(DEFAULT_DIAGNOSIS_FROM_DOCTOR);
        assertThat(testPerscription.isHomeVisit()).isEqualTo(DEFAULT_HOME_VISIT);
        assertThat(testPerscription.isReport()).isEqualTo(DEFAULT_REPORT);
    }

    @Test
    @Transactional
    public void createPerscriptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = perscriptionRepository.findAll().size();

        // Create the Perscription with an existing ID
        perscription.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPerscriptionMockMvc.perform(post("/api/perscriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(perscription)))
            .andExpect(status().isBadRequest());

        // Validate the Perscription in the database
        List<Perscription> perscriptionList = perscriptionRepository.findAll();
        assertThat(perscriptionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPerscriptions() throws Exception {
        // Initialize the database
        perscriptionRepository.saveAndFlush(perscription);

        // Get all the perscriptionList
        restPerscriptionMockMvc.perform(get("/api/perscriptions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(perscription.getId().intValue())))
            .andExpect(jsonPath("$.[*].recipieClass").value(hasItem(DEFAULT_RECIPIE_CLASS)))
            .andExpect(jsonPath("$.[*].recipieDate").value(hasItem(DEFAULT_RECIPIE_DATE.toString())))
            .andExpect(jsonPath("$.[*].latestPossibleBegin").value(hasItem(DEFAULT_LATEST_POSSIBLE_BEGIN.toString())))
            .andExpect(jsonPath("$.[*].typeOfRegulation").value(hasItem(DEFAULT_TYPE_OF_REGULATION)))
            .andExpect(jsonPath("$.[*].perscribedRemedies1").value(hasItem(DEFAULT_PERSCRIBED_REMEDIES_1)))
            .andExpect(jsonPath("$.[*].perscribedRemedies2").value(hasItem(DEFAULT_PERSCRIBED_REMEDIES_2)))
            .andExpect(jsonPath("$.[*].perscribedRemedies3").value(hasItem(DEFAULT_PERSCRIBED_REMEDIES_3)))
            .andExpect(jsonPath("$.[*].perscribedRemedies4").value(hasItem(DEFAULT_PERSCRIBED_REMEDIES_4)))
            .andExpect(jsonPath("$.[*].perscribedRemediesCount1").value(hasItem(DEFAULT_PERSCRIBED_REMEDIES_COUNT_1)))
            .andExpect(jsonPath("$.[*].perscribedRemediesCount2").value(hasItem(DEFAULT_PERSCRIBED_REMEDIES_COUNT_2)))
            .andExpect(jsonPath("$.[*].perscribedRemediesCount3").value(hasItem(DEFAULT_PERSCRIBED_REMEDIES_COUNT_3)))
            .andExpect(jsonPath("$.[*].perscribedRemediesCount4").value(hasItem(DEFAULT_PERSCRIBED_REMEDIES_COUNT_4)))
            .andExpect(jsonPath("$.[*].treatmentFrequency").value(hasItem(DEFAULT_TREATMENT_FREQUENCY)))
            .andExpect(jsonPath("$.[*].duratationTreatment").value(hasItem(DEFAULT_DURATATION_TREATMENT)))
            .andExpect(jsonPath("$.[*].indicationKey").value(hasItem(DEFAULT_INDICATION_KEY)))
            .andExpect(jsonPath("$.[*].barcodeFormat").value(hasItem(DEFAULT_BARCODE_FORMAT)))
            .andExpect(jsonPath("$.[*].colorcodeTK").value(hasItem(DEFAULT_COLORCODE_TK)))
            .andExpect(jsonPath("$.[*].icd10Code1").value(hasItem(DEFAULT_ICD_10_CODE_1)))
            .andExpect(jsonPath("$.[*].icd10Code2").value(hasItem(DEFAULT_ICD_10_CODE_2)))
            .andExpect(jsonPath("$.[*].diagnosisFromDoctor").value(hasItem(DEFAULT_DIAGNOSIS_FROM_DOCTOR)))
            .andExpect(jsonPath("$.[*].homeVisit").value(hasItem(DEFAULT_HOME_VISIT.booleanValue())))
            .andExpect(jsonPath("$.[*].report").value(hasItem(DEFAULT_REPORT.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getPerscription() throws Exception {
        // Initialize the database
        perscriptionRepository.saveAndFlush(perscription);

        // Get the perscription
        restPerscriptionMockMvc.perform(get("/api/perscriptions/{id}", perscription.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(perscription.getId().intValue()))
            .andExpect(jsonPath("$.recipieClass").value(DEFAULT_RECIPIE_CLASS))
            .andExpect(jsonPath("$.recipieDate").value(DEFAULT_RECIPIE_DATE.toString()))
            .andExpect(jsonPath("$.latestPossibleBegin").value(DEFAULT_LATEST_POSSIBLE_BEGIN.toString()))
            .andExpect(jsonPath("$.typeOfRegulation").value(DEFAULT_TYPE_OF_REGULATION))
            .andExpect(jsonPath("$.perscribedRemedies1").value(DEFAULT_PERSCRIBED_REMEDIES_1))
            .andExpect(jsonPath("$.perscribedRemedies2").value(DEFAULT_PERSCRIBED_REMEDIES_2))
            .andExpect(jsonPath("$.perscribedRemedies3").value(DEFAULT_PERSCRIBED_REMEDIES_3))
            .andExpect(jsonPath("$.perscribedRemedies4").value(DEFAULT_PERSCRIBED_REMEDIES_4))
            .andExpect(jsonPath("$.perscribedRemediesCount1").value(DEFAULT_PERSCRIBED_REMEDIES_COUNT_1))
            .andExpect(jsonPath("$.perscribedRemediesCount2").value(DEFAULT_PERSCRIBED_REMEDIES_COUNT_2))
            .andExpect(jsonPath("$.perscribedRemediesCount3").value(DEFAULT_PERSCRIBED_REMEDIES_COUNT_3))
            .andExpect(jsonPath("$.perscribedRemediesCount4").value(DEFAULT_PERSCRIBED_REMEDIES_COUNT_4))
            .andExpect(jsonPath("$.treatmentFrequency").value(DEFAULT_TREATMENT_FREQUENCY))
            .andExpect(jsonPath("$.duratationTreatment").value(DEFAULT_DURATATION_TREATMENT))
            .andExpect(jsonPath("$.indicationKey").value(DEFAULT_INDICATION_KEY))
            .andExpect(jsonPath("$.barcodeFormat").value(DEFAULT_BARCODE_FORMAT))
            .andExpect(jsonPath("$.colorcodeTK").value(DEFAULT_COLORCODE_TK))
            .andExpect(jsonPath("$.icd10Code1").value(DEFAULT_ICD_10_CODE_1))
            .andExpect(jsonPath("$.icd10Code2").value(DEFAULT_ICD_10_CODE_2))
            .andExpect(jsonPath("$.diagnosisFromDoctor").value(DEFAULT_DIAGNOSIS_FROM_DOCTOR))
            .andExpect(jsonPath("$.homeVisit").value(DEFAULT_HOME_VISIT.booleanValue()))
            .andExpect(jsonPath("$.report").value(DEFAULT_REPORT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPerscription() throws Exception {
        // Get the perscription
        restPerscriptionMockMvc.perform(get("/api/perscriptions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePerscription() throws Exception {
        // Initialize the database
        perscriptionRepository.saveAndFlush(perscription);

        int databaseSizeBeforeUpdate = perscriptionRepository.findAll().size();

        // Update the perscription
        Perscription updatedPerscription = perscriptionRepository.findById(perscription.getId()).get();
        // Disconnect from session so that the updates on updatedPerscription are not directly saved in db
        em.detach(updatedPerscription);
        updatedPerscription
            .recipieClass(UPDATED_RECIPIE_CLASS)
            .recipieDate(UPDATED_RECIPIE_DATE)
            .latestPossibleBegin(UPDATED_LATEST_POSSIBLE_BEGIN)
            .typeOfRegulation(UPDATED_TYPE_OF_REGULATION)
            .perscribedRemedies1(UPDATED_PERSCRIBED_REMEDIES_1)
            .perscribedRemedies2(UPDATED_PERSCRIBED_REMEDIES_2)
            .perscribedRemedies3(UPDATED_PERSCRIBED_REMEDIES_3)
            .perscribedRemedies4(UPDATED_PERSCRIBED_REMEDIES_4)
            .perscribedRemediesCount1(UPDATED_PERSCRIBED_REMEDIES_COUNT_1)
            .perscribedRemediesCount2(UPDATED_PERSCRIBED_REMEDIES_COUNT_2)
            .perscribedRemediesCount3(UPDATED_PERSCRIBED_REMEDIES_COUNT_3)
            .perscribedRemediesCount4(UPDATED_PERSCRIBED_REMEDIES_COUNT_4)
            .treatmentFrequency(UPDATED_TREATMENT_FREQUENCY)
            .duratationTreatment(UPDATED_DURATATION_TREATMENT)
            .indicationKey(UPDATED_INDICATION_KEY)
            .barcodeFormat(UPDATED_BARCODE_FORMAT)
            .colorcodeTK(UPDATED_COLORCODE_TK)
            .icd10Code1(UPDATED_ICD_10_CODE_1)
            .icd10Code2(UPDATED_ICD_10_CODE_2)
            .diagnosisFromDoctor(UPDATED_DIAGNOSIS_FROM_DOCTOR)
            .homeVisit(UPDATED_HOME_VISIT)
            .report(UPDATED_REPORT);

        restPerscriptionMockMvc.perform(put("/api/perscriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPerscription)))
            .andExpect(status().isOk());

        // Validate the Perscription in the database
        List<Perscription> perscriptionList = perscriptionRepository.findAll();
        assertThat(perscriptionList).hasSize(databaseSizeBeforeUpdate);
        Perscription testPerscription = perscriptionList.get(perscriptionList.size() - 1);
        assertThat(testPerscription.getRecipieClass()).isEqualTo(UPDATED_RECIPIE_CLASS);
        assertThat(testPerscription.getRecipieDate()).isEqualTo(UPDATED_RECIPIE_DATE);
        assertThat(testPerscription.getLatestPossibleBegin()).isEqualTo(UPDATED_LATEST_POSSIBLE_BEGIN);
        assertThat(testPerscription.getTypeOfRegulation()).isEqualTo(UPDATED_TYPE_OF_REGULATION);
        assertThat(testPerscription.getPerscribedRemedies1()).isEqualTo(UPDATED_PERSCRIBED_REMEDIES_1);
        assertThat(testPerscription.getPerscribedRemedies2()).isEqualTo(UPDATED_PERSCRIBED_REMEDIES_2);
        assertThat(testPerscription.getPerscribedRemedies3()).isEqualTo(UPDATED_PERSCRIBED_REMEDIES_3);
        assertThat(testPerscription.getPerscribedRemedies4()).isEqualTo(UPDATED_PERSCRIBED_REMEDIES_4);
        assertThat(testPerscription.getPerscribedRemediesCount1()).isEqualTo(UPDATED_PERSCRIBED_REMEDIES_COUNT_1);
        assertThat(testPerscription.getPerscribedRemediesCount2()).isEqualTo(UPDATED_PERSCRIBED_REMEDIES_COUNT_2);
        assertThat(testPerscription.getPerscribedRemediesCount3()).isEqualTo(UPDATED_PERSCRIBED_REMEDIES_COUNT_3);
        assertThat(testPerscription.getPerscribedRemediesCount4()).isEqualTo(UPDATED_PERSCRIBED_REMEDIES_COUNT_4);
        assertThat(testPerscription.getTreatmentFrequency()).isEqualTo(UPDATED_TREATMENT_FREQUENCY);
        assertThat(testPerscription.getDuratationTreatment()).isEqualTo(UPDATED_DURATATION_TREATMENT);
        assertThat(testPerscription.getIndicationKey()).isEqualTo(UPDATED_INDICATION_KEY);
        assertThat(testPerscription.getBarcodeFormat()).isEqualTo(UPDATED_BARCODE_FORMAT);
        assertThat(testPerscription.getColorcodeTK()).isEqualTo(UPDATED_COLORCODE_TK);
        assertThat(testPerscription.getIcd10Code1()).isEqualTo(UPDATED_ICD_10_CODE_1);
        assertThat(testPerscription.getIcd10Code2()).isEqualTo(UPDATED_ICD_10_CODE_2);
        assertThat(testPerscription.getDiagnosisFromDoctor()).isEqualTo(UPDATED_DIAGNOSIS_FROM_DOCTOR);
        assertThat(testPerscription.isHomeVisit()).isEqualTo(UPDATED_HOME_VISIT);
        assertThat(testPerscription.isReport()).isEqualTo(UPDATED_REPORT);
    }

    @Test
    @Transactional
    public void updateNonExistingPerscription() throws Exception {
        int databaseSizeBeforeUpdate = perscriptionRepository.findAll().size();

        // Create the Perscription

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPerscriptionMockMvc.perform(put("/api/perscriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(perscription)))
            .andExpect(status().isBadRequest());

        // Validate the Perscription in the database
        List<Perscription> perscriptionList = perscriptionRepository.findAll();
        assertThat(perscriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePerscription() throws Exception {
        // Initialize the database
        perscriptionRepository.saveAndFlush(perscription);

        int databaseSizeBeforeDelete = perscriptionRepository.findAll().size();

        // Delete the perscription
        restPerscriptionMockMvc.perform(delete("/api/perscriptions/{id}", perscription.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Perscription> perscriptionList = perscriptionRepository.findAll();
        assertThat(perscriptionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
