package com.holosystems.web.rest;

import com.holosystems.TheraPiApp;
import com.holosystems.domain.Doctor;
import com.holosystems.repository.DoctorRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DoctorResource} REST controller.
 */
@SpringBootTest(classes = TheraPiApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class DoctorResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_STATE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_STREET = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_STREET = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_HOUSE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_HOUSE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_ADDITIVE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_ADDITIVE = "BBBBBBBBBB";

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDoctorMockMvc;

    private Doctor doctor;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Doctor createEntity(EntityManager em) {
        Doctor doctor = new Doctor()
            .title(DEFAULT_TITLE)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .emailAddress(DEFAULT_EMAIL_ADDRESS)
            .addressCountry(DEFAULT_ADDRESS_COUNTRY)
            .addressState(DEFAULT_ADDRESS_STATE)
            .addressPostalCode(DEFAULT_ADDRESS_POSTAL_CODE)
            .addressStreet(DEFAULT_ADDRESS_STREET)
            .addressHouseNumber(DEFAULT_ADDRESS_HOUSE_NUMBER)
            .addressAdditive(DEFAULT_ADDRESS_ADDITIVE);
        return doctor;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Doctor createUpdatedEntity(EntityManager em) {
        Doctor doctor = new Doctor()
            .title(UPDATED_TITLE)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .emailAddress(UPDATED_EMAIL_ADDRESS)
            .addressCountry(UPDATED_ADDRESS_COUNTRY)
            .addressState(UPDATED_ADDRESS_STATE)
            .addressPostalCode(UPDATED_ADDRESS_POSTAL_CODE)
            .addressStreet(UPDATED_ADDRESS_STREET)
            .addressHouseNumber(UPDATED_ADDRESS_HOUSE_NUMBER)
            .addressAdditive(UPDATED_ADDRESS_ADDITIVE);
        return doctor;
    }

    @BeforeEach
    public void initTest() {
        doctor = createEntity(em);
    }

    @Test
    @Transactional
    public void createDoctor() throws Exception {
        int databaseSizeBeforeCreate = doctorRepository.findAll().size();

        // Create the Doctor
        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doctor)))
            .andExpect(status().isCreated());

        // Validate the Doctor in the database
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeCreate + 1);
        Doctor testDoctor = doctorList.get(doctorList.size() - 1);
        assertThat(testDoctor.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testDoctor.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testDoctor.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testDoctor.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testDoctor.getEmailAddress()).isEqualTo(DEFAULT_EMAIL_ADDRESS);
        assertThat(testDoctor.getAddressCountry()).isEqualTo(DEFAULT_ADDRESS_COUNTRY);
        assertThat(testDoctor.getAddressState()).isEqualTo(DEFAULT_ADDRESS_STATE);
        assertThat(testDoctor.getAddressPostalCode()).isEqualTo(DEFAULT_ADDRESS_POSTAL_CODE);
        assertThat(testDoctor.getAddressStreet()).isEqualTo(DEFAULT_ADDRESS_STREET);
        assertThat(testDoctor.getAddressHouseNumber()).isEqualTo(DEFAULT_ADDRESS_HOUSE_NUMBER);
        assertThat(testDoctor.getAddressAdditive()).isEqualTo(DEFAULT_ADDRESS_ADDITIVE);
    }

    @Test
    @Transactional
    public void createDoctorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = doctorRepository.findAll().size();

        // Create the Doctor with an existing ID
        doctor.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDoctorMockMvc.perform(post("/api/doctors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doctor)))
            .andExpect(status().isBadRequest());

        // Validate the Doctor in the database
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDoctors() throws Exception {
        // Initialize the database
        doctorRepository.saveAndFlush(doctor);

        // Get all the doctorList
        restDoctorMockMvc.perform(get("/api/doctors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doctor.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].emailAddress").value(hasItem(DEFAULT_EMAIL_ADDRESS)))
            .andExpect(jsonPath("$.[*].addressCountry").value(hasItem(DEFAULT_ADDRESS_COUNTRY)))
            .andExpect(jsonPath("$.[*].addressState").value(hasItem(DEFAULT_ADDRESS_STATE)))
            .andExpect(jsonPath("$.[*].addressPostalCode").value(hasItem(DEFAULT_ADDRESS_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].addressStreet").value(hasItem(DEFAULT_ADDRESS_STREET)))
            .andExpect(jsonPath("$.[*].addressHouseNumber").value(hasItem(DEFAULT_ADDRESS_HOUSE_NUMBER)))
            .andExpect(jsonPath("$.[*].addressAdditive").value(hasItem(DEFAULT_ADDRESS_ADDITIVE)));
    }
    
    @Test
    @Transactional
    public void getDoctor() throws Exception {
        // Initialize the database
        doctorRepository.saveAndFlush(doctor);

        // Get the doctor
        restDoctorMockMvc.perform(get("/api/doctors/{id}", doctor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(doctor.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.emailAddress").value(DEFAULT_EMAIL_ADDRESS))
            .andExpect(jsonPath("$.addressCountry").value(DEFAULT_ADDRESS_COUNTRY))
            .andExpect(jsonPath("$.addressState").value(DEFAULT_ADDRESS_STATE))
            .andExpect(jsonPath("$.addressPostalCode").value(DEFAULT_ADDRESS_POSTAL_CODE))
            .andExpect(jsonPath("$.addressStreet").value(DEFAULT_ADDRESS_STREET))
            .andExpect(jsonPath("$.addressHouseNumber").value(DEFAULT_ADDRESS_HOUSE_NUMBER))
            .andExpect(jsonPath("$.addressAdditive").value(DEFAULT_ADDRESS_ADDITIVE));
    }

    @Test
    @Transactional
    public void getNonExistingDoctor() throws Exception {
        // Get the doctor
        restDoctorMockMvc.perform(get("/api/doctors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDoctor() throws Exception {
        // Initialize the database
        doctorRepository.saveAndFlush(doctor);

        int databaseSizeBeforeUpdate = doctorRepository.findAll().size();

        // Update the doctor
        Doctor updatedDoctor = doctorRepository.findById(doctor.getId()).get();
        // Disconnect from session so that the updates on updatedDoctor are not directly saved in db
        em.detach(updatedDoctor);
        updatedDoctor
            .title(UPDATED_TITLE)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .emailAddress(UPDATED_EMAIL_ADDRESS)
            .addressCountry(UPDATED_ADDRESS_COUNTRY)
            .addressState(UPDATED_ADDRESS_STATE)
            .addressPostalCode(UPDATED_ADDRESS_POSTAL_CODE)
            .addressStreet(UPDATED_ADDRESS_STREET)
            .addressHouseNumber(UPDATED_ADDRESS_HOUSE_NUMBER)
            .addressAdditive(UPDATED_ADDRESS_ADDITIVE);

        restDoctorMockMvc.perform(put("/api/doctors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDoctor)))
            .andExpect(status().isOk());

        // Validate the Doctor in the database
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeUpdate);
        Doctor testDoctor = doctorList.get(doctorList.size() - 1);
        assertThat(testDoctor.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testDoctor.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testDoctor.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testDoctor.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testDoctor.getEmailAddress()).isEqualTo(UPDATED_EMAIL_ADDRESS);
        assertThat(testDoctor.getAddressCountry()).isEqualTo(UPDATED_ADDRESS_COUNTRY);
        assertThat(testDoctor.getAddressState()).isEqualTo(UPDATED_ADDRESS_STATE);
        assertThat(testDoctor.getAddressPostalCode()).isEqualTo(UPDATED_ADDRESS_POSTAL_CODE);
        assertThat(testDoctor.getAddressStreet()).isEqualTo(UPDATED_ADDRESS_STREET);
        assertThat(testDoctor.getAddressHouseNumber()).isEqualTo(UPDATED_ADDRESS_HOUSE_NUMBER);
        assertThat(testDoctor.getAddressAdditive()).isEqualTo(UPDATED_ADDRESS_ADDITIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingDoctor() throws Exception {
        int databaseSizeBeforeUpdate = doctorRepository.findAll().size();

        // Create the Doctor

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDoctorMockMvc.perform(put("/api/doctors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(doctor)))
            .andExpect(status().isBadRequest());

        // Validate the Doctor in the database
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDoctor() throws Exception {
        // Initialize the database
        doctorRepository.saveAndFlush(doctor);

        int databaseSizeBeforeDelete = doctorRepository.findAll().size();

        // Delete the doctor
        restDoctorMockMvc.perform(delete("/api/doctors/{id}", doctor.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Doctor> doctorList = doctorRepository.findAll();
        assertThat(doctorList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
