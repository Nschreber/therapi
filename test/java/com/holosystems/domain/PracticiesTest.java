package com.holosystems.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.holosystems.web.rest.TestUtil;

public class PracticiesTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Practicies.class);
        Practicies practicies1 = new Practicies();
        practicies1.setId(1L);
        Practicies practicies2 = new Practicies();
        practicies2.setId(practicies1.getId());
        assertThat(practicies1).isEqualTo(practicies2);
        practicies2.setId(2L);
        assertThat(practicies1).isNotEqualTo(practicies2);
        practicies1.setId(null);
        assertThat(practicies1).isNotEqualTo(practicies2);
    }
}
