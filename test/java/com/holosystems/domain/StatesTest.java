package com.holosystems.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.holosystems.web.rest.TestUtil;

public class StatesTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(States.class);
        States states1 = new States();
        states1.setId(1L);
        States states2 = new States();
        states2.setId(states1.getId());
        assertThat(states1).isEqualTo(states2);
        states2.setId(2L);
        assertThat(states1).isNotEqualTo(states2);
        states1.setId(null);
        assertThat(states1).isNotEqualTo(states2);
    }
}
