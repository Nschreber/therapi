package com.holosystems.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.holosystems.web.rest.TestUtil;

public class PerscriptionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Perscription.class);
        Perscription perscription1 = new Perscription();
        perscription1.setId(1L);
        Perscription perscription2 = new Perscription();
        perscription2.setId(perscription1.getId());
        assertThat(perscription1).isEqualTo(perscription2);
        perscription2.setId(2L);
        assertThat(perscription1).isNotEqualTo(perscription2);
        perscription1.setId(null);
        assertThat(perscription1).isNotEqualTo(perscription2);
    }
}
